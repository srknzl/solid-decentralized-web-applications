graph TB
    id1([Offline stories URI])---|object|S9
    id2(['OFS'])---|name|S9
    id3(['OFS.size>0'])---|condition|S4
    id5([Offline Stories URI])---|object|S6
    id6([Human input])---|index|S6
    id7(['story'])---|name|S10
    id8(['story.count + 1'])---|condition|S7
    id9(['count'])---|dataField|S8

    S9[Save].->S4[Evaluate]
    S4.->|false|S5[Display]
    S4.->|true|S6[Get]
    S6-->|object|S10[Save]
    S10.->S7[Evaluate]
    S7-->|value|S8[Modify]
    S6-->|object|S8
    id4(['No offline Stories'])---|message|S5


    classDef inputPort fill:#f90,stroke:#333,stroke-width:2px;
    classDef step fill:#09f,stroke:#111,stroke-width:2px,color: white;
    classDef humanInput fill:#090,stroke:#111,stroke-width:2px,color: white;
    class id1,id2,id3,id4,id5,id7,id8,id9 inputPort;
    class S4,S6,S5,S7,S8,S9,S10 step;
    class id6 humanInput;

    classDef inputPort fill:#f90,stroke:#333,stroke-width:2px;
    classDef step fill:#09f,stroke:#111,stroke-width:2px,color: white;
    classDef humanInput fill:#090,stroke:#111,stroke-width:2px,color: white;
    class id1,id2,id3,id4,id5,id7,id8,id9 inputPort;
    class S4,S6,S5,S7,S8,S9,S10 step;
    class id6 humanInput;


This is a mermaid.js diagram.




graph TB
    st([Stories])---SeS[Select a story]
    hi[User input]---SeS
    SeS---RtS[Rate the Story]
    hi2[User input]---RtS



    class st inputPort;
    class SeS,RtS step;
    class hi,hi2 humanInput;

    classDef inputPort fill:#f90,stroke:#333,stroke-width:2px;
    classDef step fill:#09f,stroke:#111,stroke-width:2px,color: white;
    classDef humanInput fill:#090,stroke:#111,stroke-width:2px,color: white;

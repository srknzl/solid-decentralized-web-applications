const $rdf = require("rdflib");

const store = $rdf.graph();
const me = store.sym("https://example.com/alice/card#me");
const profile = me.doc(); //    i.e. store.sym(''https://example.com/alice/card#me');
const VCARD = new $rdf.Namespace("http://www.w3.org/2006/vcard/ns#");
const fetcher = new $rdf.Fetcher(store);
fetcher.load(profile).then(
  response => {
    let name = store.any(me, VCARD("fn"));
    console.log(`Loaded ${$name || "wot no name?"}`);
  },
  err => {
    console.log("Load failed " + err);
  }
);
const FOAF = $rdf.Namespace('http://xmlns.com/foaf/0.1/');
let name = store.any(me, VCARD('fn')) || store.any(me, FOAF('name'));
let picture = store.any(me, VCARD('hasPhoto')) || store.any(me, FOAF(image));

let names = store.each(me, VCARD('fn')).concat(store.each(me, FOAF('name')));

import Vue from 'vue';
import Vuex from 'vuex';
const $rdf = require("rdflib");
import axios from "axios";


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    datatypes: [],
    dataInstances: [],
    workflows: [],
    workflowInstances: [],
    steps: [],
    stepInstances: [],
    users: [],
    statements: [],
    fetching: false,
    store: $rdf.graph(),
    apiEndpoint: "http://localhost:3000/api",
    containers: [
      "/datatypes",
      "/data_instances",
      "/steps",
      "/step_instances",
      "/users",
      "/workflows",
      "/workflow_instances"
    ]
  },
  mutations: {
    clear(state) {
      state.store = $rdf.graph();
      state.datatypes = [];
      state.dataInstances = [];
      state.workflows = [];
      state.workflowInstances = [];
      state.steps = [];
      state.stepInstances = [];
      state.users = [];
      state.statements = [];
    },
    refresh(state) {
      state.statements = state.store.match();
      state.datatypes = state.store.match(
        state.store.sym(state.apiEndpoint + "/datatypes"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
      state.dataInstances = state.store.match(
        state.store.sym(state.apiEndpoint + "/data_instances"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
      state.steps = state.store.match(
        state.store.sym(state.apiEndpoint + "/steps"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
      state.stepInstances = state.store.match(
        state.store.sym(state.apiEndpoint + "/step_instances"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
      state.workflows = state.store.match(
        state.store.sym(state.apiEndpoint + "/workflows"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
      state.workflowInstances = state.store.match(
        state.store.sym(state.apiEndpoint + "/workflow_instances"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
      state.users = state.store.match(
        state.store.sym(state.apiEndpoint + "/users"),
        state.store.sym("http://www.w3.org/ns/ldp#contains")
      );
    },
    updateapiEndpoint(state, value) {
      state.apiEndpoint = value;
    }
  },
  actions: {
    init({ commit, state }) {
      state.fetching = true;
      const promises = [];
      state.containers.forEach(endpoint => {
        promises.push(
          axios
            .get(state.apiEndpoint + endpoint)
            .then(res => {
              $rdf.parse(
                res.data.text,
                state.store,
                $rdf.sym(state.apiEndpoint + endpoint).uri,
                "text/turtle"
              );
            })
            .catch(err => {
              console.log(err);
            })
        );
      });
      Promise.all(promises).then(() => {
        state.fetching = false;
        commit("refresh");
      }).catch(err => {
        console.log(err);
        state.fetching = false;
      });
    },
    invokeWorkflow({ commit, state }, payload) {
      const promises = [];
      promises.push(axios
        .post(payload.workflow.value)
        .then(res => {
          payload.vm.$bvToast.toast("Generated workflow instance at: " + res.data.location, {
            title: 'Workflow invoked',
            autoHideDelay: 2000,
            appendToast: false
          });
          $rdf.parse(
            res.data.text,
            state.store,
            $rdf.sym(payload.workflow.value).uri,
            "text/turtle"
          );
        })
        .catch(err => {
          console.log(err);
        }));
      Promise.all(promises).then(() => {
        this.dispatch("init")
      });


    }
  }
});

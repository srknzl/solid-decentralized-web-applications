const sparql = require("sparql");

client = new sparql.Client("http://dbpedia.org/sparql");

client.rows("select * where { ?s ?p ?o } limit 1000000000000", (err, res) => {
  if (!err) {
    res.forEach(row => {
      console.log(
        " Subject: " + row.s.value + "\n",
        "Predicate: " + row.p.value + "\n",
        "Object:" + row.o.value + "\n"
      );
    });
  } else console.log(err);
});

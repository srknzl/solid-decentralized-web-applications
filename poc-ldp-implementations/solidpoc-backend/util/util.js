const { exec } = require("child_process");
const path = require("path");


const sparqlClient = require("./sparqlClient");
const settings = require("./settings");

const parseGraph = (res) => {
  exec(`riot --rdfs="${path.join(__dirname, '..' , 'graph', 'poc_core.ttl')}" "${path.join(__dirname, "..", "spec.ttl")}"`, (error, stdout, stderr) => {
    if (error) {
      res.sendFile(path.join(__dirname,'..', "templates", 'error_spec.html'));
      console.error(error);
      
      return;
    }
    if (stderr) {
      res.sendFile(path.join(__dirname,'..', "templates", 'error_spec.html'));
      console.error(stderr);
      return;
    }
    query = `
    DELETE {GRAPH <${settings.dataUri}> {?s ?p ?o}} WHERE {GRAPH <${settings.dataUri}> {?s ?p ?o}};
    INSERT DATA {
      GRAPH <${settings.dataUri}> {
        ${stdout}
      }
      };`
    sparqlClient.getSimpleClient().query.update(query);
  });
}

module.exports = {parseGraph: parseGraph};


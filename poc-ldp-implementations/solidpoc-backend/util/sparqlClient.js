const simpleSparqlClient = require("sparql-http-client/SimpleClient");
const streamSparqlClient = require("sparql-http-client");

let simpleClient;
let streamClient;

const getSimpleClient = () => {
  if(!simpleClient){
    sC = new simpleSparqlClient({
      endpointUrl: "http://localhost:3030/ds/sparql",
      updateUrl: "http://localhost:3030/ds/update",
      storeUrl: "http://localhost:3030/ds/data",
    });
    simpleClient = sC;
    return simpleClient;
  }else return simpleClient;
};


const getStreamClient = () => {
  if(!streamClient){
    sC = new streamSparqlClient({
      endpointUrl: "http://localhost:3030/ds/sparql",
      updateUrl: "http://localhost:3030/ds/update",
      storeUrl: "http://localhost:3030/ds/data",
    });
    streamClient = sC;
    return streamClient;
  }else return streamClient;
};

module.exports = {
  getSimpleClient: getSimpleClient,
  getStreamClient: getStreamClient
}
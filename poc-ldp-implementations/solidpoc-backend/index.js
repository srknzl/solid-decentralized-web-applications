const express = require("express");
const bodyParser = require("body-parser");
const multer = require('multer');

const fs = require("fs");
const path = require("path");

const apiRouter = require("./router/apiRouter");
const util = require("./util/util");


const upload = multer({ storage: multer.memoryStorage() });
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.get("/", (req, res, next) => { // spec upload page
  res.sendFile(path.join(__dirname, "templates", 'index.html'));
});

app.use("/api", apiRouter); // route about the api

app.post("/upload-spec", upload.single('spec'), (req, res, next) => {
  const spec = req.file;
  fs.writeFileSync("spec.ttl", spec.buffer);
  util.parseGraph(res);
  res.sendFile(path.join(__dirname, "templates", "spec-loaded.html"));
});



const listener = app.listen(3000, () => {
  console.log("The app is listening on port " + listener.address().port);
});

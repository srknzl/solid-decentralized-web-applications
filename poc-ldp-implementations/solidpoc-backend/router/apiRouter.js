const express = require("express");
const sparqlClient = require("../util/sparqlClient");
const router = express.Router();

router.get("/", (req, res, next) => {
  res.json("Welcome");
});

router.post('/registerUser', async (req, res, next) => {
  const userIRI = req.body.userIRI;
  const updateQuery = `
  PREFIX services: <http://soslab.cmpe.boun.edu.tr/ontologies/poc_services.ttl#> 

  INSERT DATA {
    GRAPH <http://poc.users> {
      <${userIRI}> a services:User .
      } 
    }`;
  try {
    await sparqlClient.getSimpleClient().query.update(updateQuery);
    res.status(200).json("OK");
  } catch (error) {
    res.status(500).json(JSON.stringify(error));
  }

});

/* router.get('/test', async (req, res, next) => {
  let hostname = "srknzl.solid.community";
  try {
    let session = await auth.currentSession();
    if (!session) {
      session = await auth.login({
        idp: "https://solid.community",
        username: "srknzl",
        password: "3"
      });
    }

    fc.readFolder(`https://${hostname}/pocSolid/`)
      .then(resa => {
        console.log(resa);
        res.status(200).json("OK");
      })
      .catch(err => {
        console.log(err);
        res.status(500).json(JSON.stringify(err));

      });
    /* axios.get("https://srknzl.solid.community/pocSolid/1.txt", {
      headers: {
        "Origin": "http://localhost:3000"
      }
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      }); 
  } catch (error) {
    console.log(error);
    res.status(500).json(JSON.stringify(error));
  }

}); */


module.exports = router;
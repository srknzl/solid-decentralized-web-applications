module POC
  module Test
    module UDDs
      def self.read_data_instance
        udd = CompositeDatatype.find('http://example.org/haircuts#Haircut')
        raise 'Datatype not loaded properly' if udd.nil? || udd.title != 'Haircut' || udd.data_fields.count != 4

        uddi = CompositeDataInstance.find('http://example.org/haircuts#H01')
        raise 'Data instance not loaded properly.' if uddi.nil? || uddi.field_values.count != 4
        df = uddi.field_values.find {|fv| fv.value == 'http://example.org/haircuts#HC01'}
        raise 'Data fields not loaded properly.' if df.nil?

        ii = Image.find(df.value.to_s)
        raise 'ImageType instance not loaded properly' unless ii.path.uri?
        true
      end

      def self.image_duplication
        image = Image.find('http://example.org/haircuts#Photo0')
        raise 'Image not found!' unless image
        init_num_images = Image.count

        workflow = Workflow.find('http://example.org/haircuts#ImageDuplication')
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance

        raise 'Not completed!' unless wf_instance.completed?

        num_images = Image.count
        duplicate_image = Image.all.resources.find { |i| i.duplicate_of?(image) }

        raise 'Image duplication failed!' if num_images != init_num_images + 1 || duplicate_image.nil?
        true
      end

      def self.uddi_duplication
        instance = CompositeDataInstance.find('http://example.org/haircuts#Haircut0')
        raise 'Instance not found!' unless instance
        init_num_instances = CompositeDataInstance.count

        workflow = Workflow.find('http://example.org/haircuts#HaircutDuplication')
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance

        raise 'Not completed!' unless wf_instance.completed?

        num_instances = CompositeDataInstance.count
        duplicate_instance = CompositeDataInstance.all.resources.find { |i| i.duplicate_of?(instance) }

        raise 'UDDI duplication failed!' if num_instances != init_num_instances + 1 || duplicate_instance.nil?
        true
      end


    end

  end
end

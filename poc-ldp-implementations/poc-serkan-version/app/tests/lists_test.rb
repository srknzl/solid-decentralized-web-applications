module POC
  module Test
    module Lists

      def self.append
        list = List.find('http://example.org/ttt#list0')
        raise 'List not found!' unless list
        initial_size = list.size

        workflow = Workflow.find('http://example.org/ttt#AppendTest', cached: true)
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance
        # p wf_instance.status.class
        raise 'Not completed!' unless wf_instance.completed?


        list = List.find('http://example.org/ttt#list0')
        raise 'Append failed!' unless  (list.size == initial_size + 1) and list[list.size-1] == 'my item'
        true
      end


      def self.prepend
        list = List.find('http://example.org/ttt#list0')
        raise 'List not found!' unless list

        workflow = Workflow.find('http://example.org/ttt#PrependTest')
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance
        # p wf_instance.status.class
        raise 'Not completed!' unless wf_instance.completed?

        raise 'Prepend failed!' unless  list.size > 0 and list[0] == 'another item'

        true
      end


      def self.removal
        list = List.find('http://example.org/ttt#list1', cached: true)
        raise 'List not found!' unless list
        initial_size = list.size
        raise 'List is empty!' unless initial_size > 0

        workflow = Workflow.find('http://example.org/ttt#ItemRemoval', cached: true)
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance
        # p wf_instance.status.class
        raise 'Not completed!' unless wf_instance.completed?

        list = List.find('http://example.org/ttt#list1', cached: true)
        p 'Removal failed! This does not indicate that the functionality is broken. You may have tried to remove a non-existing item.' unless  list.size < initial_size # What if the list does not contain the element?

        true
      end

      def self.get_and_insert
        list1 = List.find('http://example.org/ttt#list1', cached: true)
        raise 'List not found!' unless list1
        raise 'list1 need to have a size of at least 2!' if list1.size < 2

        list2 = List.find('http://example.org/ttt#list2', cached: true)
        raise 'List not found!' unless list2
        initial_size = list2.size
        workflow = Workflow.find('http://example.org/ttt#GetAndInsert', cached: true)
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance
        # p wf_instance.status.class
        raise 'Not completed!' unless wf_instance.completed?

        updated_list2 = List.find('http://example.org/ttt#list2', cached: true)
        raise 'Failed!' unless updated_list2.last == list1[1] and updated_list2.size == initial_size + 1

        true
      end

      def self.duplication
        instance = List.find('http://example.org/ttt#list1', cached: true)
        raise 'List not found!' unless instance
        init_num_instances = List.count
        workflow = Workflow.find('http://example.org/ttt#ListDuplication', cached: true)
        raise 'Workflow not found!' unless workflow

        wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

        raise 'WorkflowInstance not created!' unless wf_instance
        raise 'Not completed!' unless wf_instance.completed?

        duplicate_instance = List.all.resources.find { |i| i.duplicate_of?(instance) }

        num_instances = List.count
        raise 'List duplication failed!' if num_instances != init_num_instances + 1 || duplicate_instance.nil?
        true
      end


    end
  end
end

module POC
  module Test
    def self.run(method)
      init_time = Time.current
      eval(method)
      p "Time spent: #{(Time.current - init_time)} seconds."
    end
  end

end

# TODO: Usage of a class variable to store the current user conflicts with our multi-tenant architecture.
# TODO: This shall be fixed along with an authentication mechanism.

module POC
  module Auxiliary
    extend Tripod::Finders::ClassMethods

    CONCEPT_CLASSES = [:Workflow, :Step, :Role, :Constraint, :DataInstance, :Port, :Datatype]

    class << self

      def get_from_redis(uri)
        Redis::Value.new(uri.to_s, marshal:true).value
      end

      def flush_auxiliary_graph
        flush_graph(SETTINGS.auxiliary_data_uri)
      end

      def flush_data_graph
        flush_graph(SETTINGS.data_uri)
      end

      def flush_graph(graph_uri)
        @@current_user = nil
        query = "
        DELETE {GRAPH <#{graph_uri.to_s}> {?s ?p ?o}} WHERE {GRAPH <#{graph_uri.to_s}> {?s ?p ?o}};
      "
        sparql_update(query)
      end

      def flush_database
        @@current_user = nil
        query = 'DELETE {GRAPH ?g {?s ?p ?o}} WHERE {GRAPH ?g {?s ?p ?o}};'
        sparql_update(query)
      end

      def flush_cache
        Redis::Objects.redis.keys.each do |key|
          Redis::Objects.redis.del(key)
        end
      end

      def replace_uri_in_graph!(graph, uri, new_uri)
        graph.query(subject: uri) do |s|
          graph.delete s
          graph << RDF::Statement.new(new_uri, s.predicate, s.object)
        end

        graph.query(object: uri) do |s|
          graph.delete s
          graph << RDF::Statement.new(s.subject, s.predicate, new_uri)
        end
        graph
      end

      def replace_uri_in_graph(graph, uri, new_uri)
        new_graph = RDF::Graph.new
        new_graph << graph
        replace_uri_in_graph!(new_graph, uri, new_uri)
      end

      def sparql_update(query)
        puts 'Sparql update to be executed:'
        puts query
        Tripod::SparqlClient::Update::update(query)
      end

      def sparql_query(query)
        puts 'Sparql query to be executed:'
        puts query
        Tripod::SparqlClient::Query.query(query, "application/n-triples")
      end

      # Returns a RDF::URI based on the string +name+.
      # Returns a RDF::URI based on a name if provided, otherwise uses the name of the calling object.
      # # @example
      #   Concept.generate_uri
      def generate_uri(name = self.name, base_uri=SETTINGS.data_uri)
        klass_s = name.underscore
        klass_p = klass_s.pluralize
        RDF::URI("#{base_uri}#{SETTINGS.api_namespace}/#{klass_p}/#{klass_s}_#{generate_suffix}")
      end

      def time_stamp
        Time.current.strftime("%m%d%H%M%S%L")
      end

      def random_string(length = 10)
        (('a'..'z').to_a + (0..9).to_a).shuffle[0, length].join.to_s
      end

      def generate_suffix
        "#{time_stamp}_#{random_string}"
      end

      # Performs BNode skolemization on +graph+, using +base_uri+ as the base of the URIs to be generated.
      # We need BNode skolemization  of our data store to give the specifications the flexibility to use blank nodes to refer various constructs, such as Step instances.
      # @example
      #   ..
      def skolemize!(graph, base_uri=SETTINGS.data_uri)
        graph.each do |s|
          if s.subject.is_a?(RDF::Node)
            klass = _matched_concept_class(graph, s.subject)
            name = klass.present? ? klass.name : 'ReplacedNode'
            uri = RDF::URI(generate_uri(name, base_uri))
            replace_uri_in_graph!(graph, s.subject, uri)
          end
        end
        graph
      end

      def _matched_concept_class(graph, node)
        CONCEPT_CLASSES.map { |c| c.to_s.constantize }.each do |klass|
          return klass if graph.query(subject: node, predicate: RDF.type, object: klass.get_rdf_type).count > 0
        end
        false
      end
      protected :_matched_concept_class


      # TODO: To be removed when the user stuff is handled!
      def current_user
        Redis::Value.new('current_user', marshal:true).value || create_dummy_current_user
      end

      def create_dummy_current_user
        User.all.resources.each(&:destroy)
        new_user = User.new
        new_user.save!
        self.current_user = new_user
        new_user
      end

      def current_user=(new_user)
        current_user = Redis::Value.new('current_user', marshal:true)
        current_user.value = new_user
      end

    end
  end
end

A = POC::Auxiliary
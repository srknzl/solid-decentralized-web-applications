require Rails.root.join('app', 'core', 'step_execution', 'base.rb')

module StepExecution::CreateStep
  extend StepExecution::Base

  def self.execute(inputs)
    object = retrieve_input 'object', inputs, true
    datatype = retrieve_input 'datatype', inputs, true

    if datatype.value.belongs_to_xsd?
      value = object.value
    else
      instance = DataInstance.find object.value#, cached: true
      if datatype.value == instance._datatype_uri || instance.rdf_type.include?(datatype.value)
        instance_class = DataInstance.find_descendant instance.rdf_type
      else
        raise 'Datatype of the object does not match with the expected datatype.'
      end

      raise 'Instance class not found!' unless instance_class
      instance.remove_if_cached
      instance = instance_class.find(object.value)

      new_object = instance.duplicate
      raise 'Object not created properly!' if new_object.nil? || !new_object.duplicate_of?(instance)
      new_object.save!
      value = new_object.uri

    end

    [ValueBinding.new(name: 'result', value: value)]
  end
end

require Rails.root.join('app', 'core', 'step_execution', 'base.rb')

class StepExecution::RemoveStep
  extend StepExecution::Base

  def self.execute(inputs)

    source = retrieve_input 'source', inputs, true
    object = retrieve_input 'object', inputs
    index = retrieve_input 'index', inputs

    raise 'Object or index must be specified for removal!' unless object || index
    arg = {}
    arg[:object] = object.value if object
    arg[:index] = index.value if index

    list = List.find(source.value, cached: true)
    list.remove arg
    list.save!

    [ValueBinding.new(name: 'result', value: list.uri)]
  end

end
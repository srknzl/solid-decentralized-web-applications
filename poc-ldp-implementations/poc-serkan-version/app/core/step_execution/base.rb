module StepExecution
  module Base
    def retrieve_input(name, inputs, is_required=false)
      input = inputs.find { |vb| vb.label == name }
      raise "Input '#{name}' not set properly!" if is_required and !input
      input
    end
  end
end


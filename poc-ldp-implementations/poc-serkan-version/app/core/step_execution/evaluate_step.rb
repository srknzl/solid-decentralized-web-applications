require Rails.root.join('app', 'core', 'step_execution', 'base.rb')

module StepExecution::EvaluateStep
  extend StepExecution::Base

  def self.execute(inputs)

    expression = retrieve_input 'expression', inputs
    # index = retrieve_input 'index', inputs

    # Retrieve the variables in the expression.
    variable_labels = expression.value.scan(/@[\w.]+/).map! { |s| s.tr('@','') }

    # variables = variable_labels.map { |label| retrieve_input label, inputs }

    [ValueBinding.new(name: 'result', value: true)]
  end

end

module StepExecution::Engine
  module_function

  def execute(inputs, rdf_types)
    if rdf_types.include? POC_CORE.CreateStep
      StepExecution::CreateStep.execute(inputs)
    elsif rdf_types.include? POC_CORE.InsertStep
      StepExecution::InsertStep.execute(inputs)
    elsif rdf_types.include? POC_CORE.RemoveStep
      StepExecution::RemoveStep.execute(inputs)
    elsif rdf_types.include? POC_CORE.GetStep
      StepExecution::GetStep.execute(inputs)
    elsif rdf_types.include? POC_CORE.SaveStep
      StepExecution::SaveStep.execute(inputs)
    elsif rdf_types.include? POC_CORE.DeleteStep
      StepExecution::DeleteStep.execute(inputs)
    else
      raise 'Step not recognized.'
    end
  end

end

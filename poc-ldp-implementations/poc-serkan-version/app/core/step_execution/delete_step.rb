require Rails.root.join('app', 'core', 'step_execution', 'base.rb')

module StepExecution::DeleteStep
  extend StepExecution::Base

  def self.execute(inputs)
    object = retrieve_input 'object', inputs, true

    raise 'Literal values cannot be deleted!' unless object.value.is_a? RDF::URI

    data_instance = DataInstance.find object.value

    data_instance = data_instance.get_most_specific_object
    data_instance.saved = false
    data_instance.save!

    []
  end
end

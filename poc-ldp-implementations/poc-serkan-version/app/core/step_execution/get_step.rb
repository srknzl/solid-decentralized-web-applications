require Rails.root.join('app', 'core', 'step_execution', 'base.rb')

module StepExecution::GetStep
  extend StepExecution::Base

  def self.execute(inputs)

    source = retrieve_input 'source', inputs
    index = retrieve_input 'index', inputs

    list = List.find(source.value, cached: true)

    [ValueBinding.new(name: 'result', value: list.items[index.value])]
  end

end

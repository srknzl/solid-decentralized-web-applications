require Rails.root.join('app', 'core', 'step_execution', 'base.rb')

class StepExecution::InsertStep
  extend StepExecution::Base

  def self.execute(inputs)

    target = retrieve_input 'target', inputs, true
    object = retrieve_input 'object', inputs, true
    index = retrieve_input 'index', inputs

    list = List.find(target.value, cached: true)
    arg = {}

    raise 'Object needed for insertion!' if !object || !object.value
    arg[:item] = object.value

    if index
      arg[:index] = index.value
    end

    list.insert arg
    list.save!

    [ValueBinding.new(name: 'result', value: list.uri)] # TODO: Decide. Are we passing objects or URIs? Why?
  end

end
require Rails.root.join('app','core','ldp','resource.rb')

module POC::LDP
  class ErrorResource < POC::LDP::Resource

    def initialize(opts={})
      super(opts)
      self.graph << RDF::Statement.new( RDF::URI.new(''), RDF.type, POC_SERVICES.Error)

      if opts[:description]
        self.graph << RDF::Statement.new( RDF::URI.new(''), DCTERMS.description, opts[:description])
      end

    end

  end
end

# Represents POC-LDP Resources which are (ideally) LDPRSs (Linked Data Platform RDF Sources) [http://www.w3.org/TR/ldp/#ldpr].
module POC
  module LDP

    class Resource

      attr_accessor :graph
      attr_accessor :base_uri


      # Instantiate a +LDP RDF Source (LDP-RS)+.
      #
      # @example Instantiate a new +Resource+.
      #   Resource.new
      #
      # @param [ Hash ] opts An options hash.
      #
      # @return [ Resource ] A new +Resource+.
      def initialize(opts={})
        raise 'The parameter must be a hash!' unless opts.is_a?(Hash)
        self.graph = opts[:graph] || RDF::Graph.new
        self.base_uri = if opts[:base_uri].present?
                          opts[:base_uri].is_a?(RDF::URI) ? opts[:base_uri] : RDF::URI(opts[:base_uri])
                        else
                            RDF::URI('')
                        end
      end

      def to_ttl
        # self.graph.to_ttl

        # There seems to be a bug in +RDF::Graph.to_ttl+ method.
        # Here we manually serialize the graph, then load it back into a +RDF::Graph+, before calling the serialization method.
        # If we don't do this, graphs containing RDF Lists cannot be serialized properly.
        RDF::Graph.new.from_ttl(self.graph.to_a.map(&:to_s).inject('') { |str, s| "#{str} #{s}" }).to_ttl
      end

      # def make_invokable
      #   self.graph << RDF::Statement(self.base_uri, POC_SERVICES.action, RDF::URI("#{self.base_uri.to_s}/invoke"))
      # end

    end
  end
end
require Rails.root.join('app','core','ldp','resource.rb')

# Represents POC-LDP Resources which are (ideally) LDP-BCs (Linked Data Platform Basic Containers) [http://www.w3.org/TR/ldp/#dfn-linked-data-platform-basic-container].
module POC::LDP
  class BasicContainer < POC::LDP::Resource

    # Instantiate a +LDP Basic Container (LDP-BC)+.
    #
    # @example Instantiate a new +BasicContainer+.
    #   BasicContainer.new
    #
    # @param [ Hash ] opts An options hash.
    #
    # @return [ BasicContainer ] A new +BasicContainer+.
    def initialize(opts={})
      super(opts)
      self.graph <<  RDF::Statement.new( self.base_uri, RDF.type, LDP.BasicContainer)
    end

    def <<(uri)
      self.graph << RDF::Statement.new( self.base_uri, LDP.contains, uri)
      self
    end

  end
end

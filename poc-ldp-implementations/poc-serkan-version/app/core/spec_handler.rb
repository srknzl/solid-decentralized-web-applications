class SpecHandler
  attr_accessor :graph
  attr_accessor :base_uri
  attr_accessor :graph_uri

  def initialize(opts = {})
    self.graph = RDF::Graph.new

    load_base_graph(Rails.root.join("lib", "graphs", "poc_core.ttl"))
    load_base_graph(Rails.root.join("lib", "graphs", "poc_services.ttl"))

    self.base_uri = opts[:base_uri] || SETTINGS.data_uri
    self.graph_uri = self.base_uri
  end

  def load_base_graph(path)
    self.graph.load(path, format: :ttl)
  end

  # # Loads the graph serialized in +str+ (in Turtle format).
  # def load(str)
  #   add_ttl str
  #   skolemize!
  #   declare_data_instances_saved
  #   save_graph
  # end

  # Loads the graph from the uploaded file.
  def load_uploaded_file(uploaded_file)
    FileUtils.cp uploaded_file.tempfile, Rails.root.join("lib", "graphs", "spec.ttl")
    spec_ttl = `riot --rdfs=#{Rails.root.join("lib", "graphs", "poc_core.ttl")} #{Rails.root.join("lib", "graphs", "spec.ttl")}`
    add_ttl(spec_ttl)
    skolemize!
    declare_data_instances_saved
    save_graph
  end

  def declare_data_instances_saved
    self.graph.query(predicate: RDF.type, object: POC_CORE.DataInstance) do |statement|
      self.graph << RDF::Statement.new(statement.subject, POC_PROTOTYPE.saved, true)
    end
  end

  # Loads the graph +graph+.
  def load_graph(graph)
    self.load(graph.to_ttl) # CAUTION: +to_ttl+ cannot serialize graphs with lists AND blank nodes. It may be a bug.
  end

  def skolemize!
    POC::Auxiliary.skolemize!(self.graph, self.base_uri)
  end

  def add_ttl(str)
    RDF::Reader.for(:ttl).new(str) do |reader|
      reader.each_statement do |statement|
        self.graph << statement
      end
    end
    # RDF::Turtle::Reader.new(content) do |reader|
    #   self.graph = RDF::Graph.new.send(:insert_statements, reader)
    # end
  end

  def save_graph
    query = "
    DELETE {GRAPH <#{self.graph_uri.to_s}> {?s ?p ?o}} WHERE {GRAPH <#{self.graph_uri.to_s}> {?s ?p ?o}};
    INSERT DATA {
      GRAPH <#{self.graph_uri.to_s}> {
        #{self.graph.dump(:ntriples)}
      }
      };"
    puts query

    Tripod::SparqlClient::Update::update(query)
  end

  protected :skolemize!, :add_ttl, :save_graph
end

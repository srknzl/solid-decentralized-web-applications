module POC
  module Errors
    # class FriendlyFieldError < Exception
    #   attr_accessor :field_errors
    #   def initialize(field_errors)
    #     super
    #     @field_errors = field_errors
    #   end
    # end

    class AuthenticationError < Exception; end
    class AuthorizationError < Exception; end
    class RoutingError < Exception; end
    class UnexpectedError < Exception; end
    class ResourceNotFoundError < Exception; end
  end

end

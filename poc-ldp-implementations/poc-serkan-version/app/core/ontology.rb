module POC
  class Ontology

    attr_reader :namespace

    def uri(concept_name)
      self.namespace.present? ? self.namespace[concept_name.to_sym] : nil
    end

    def initialize(uri)
      @namespace = RDF::Vocabulary.new(uri)
    end

    def method_missing(method, *args, &block)
      if method =~ /^([a-zA-Z0-9_]+)$/ # The accepted concept names consist of letters and '_', which should cover the concept names in the the ontologies.
        self.uri(method.to_s)
      else
        super(method, *args, &block)
      end
    end

    def respond_to?(method)
      method =~ /^(.+)(?!=)$/ || super
    end

  end
end

POC_CORE = POC::Ontology.new('http://web.cmpe.boun.edu.tr/soslab/ontologies/poc#')
POC_SERVICES = POC::Ontology.new('http://web.cmpe.boun.edu.tr/soslab/ontologies/poc/services#')
POC_PROTOTYPE = POC::Ontology.new('poc://prototype#')
COLLECTIONS = POC::Ontology.new('http://purl.org/co/')
OWL = POC::Ontology.new('http://www.w3.org/2002/07/owl#')
DC = POC::Ontology.new('http://purl.org/dc/elements/1.1/')
DCTERMS = POC::Ontology.new('http://purl.org/dc/terms/')
RDFS = POC::Ontology.new(RDF::RDFS.to_s)
LDP = POC::Ontology.new('http://www.w3.org/ns/ldp#')
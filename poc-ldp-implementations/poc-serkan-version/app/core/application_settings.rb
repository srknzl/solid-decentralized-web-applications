class ApplicationSettings < Hash

  def method_missing(method, *args, &block)
    if method =~ /^(.+)=$/ and args.size == 1
      self[($1).to_sym]= args[0]
    elsif args.empty?
      self[method.to_sym]
    else
      super(method, *args, &block)
    end
  end

  def respond_to?(method)
    method.present? || super
  end

end

SETTINGS = ApplicationSettings.new

SETTINGS.data_uri = 'http://poc.data'
SETTINGS.api_namespace = '/api'
SETTINGS.auxiliary_data_uri = 'http://soslab.cmpe.boun.edu.tr/ontologies/poc_core.ttl'

class StepInstance < Instance
  rdf_type POC_SERVICES.StepInstance.to_s
  field :status, POC_SERVICES.status.to_s

  linked_field :step, POC_CORE.datatype, class_name: 'Step' # TODO: Fields based on the same predicate might cause a problem. See WorkflowInstance (:workflow).
  # linked_field :step, POC_CORE.step, class_name: 'Step'
  linked_field :inputs, POC_SERVICES.input, class_name: 'ValueBinding', multivalued: true
  linked_field :outputs, POC_SERVICES.output, class_name: 'ValueBinding', multivalued: true
  linked_back :workflow_instance, :step_instances, class_name: 'WorkflowInstance'
  # linked_from :workflow_instances, :_step_instances, class_name: 'WorkflowInstance'

  # set_callback :save, :before, :save_constraints
  # set_callback :destroy, :before, :destroy_constraints

  def initialize(*args)
    super(*args)
    if args.size > 0 && args.first.is_a?(Hash) && args.first.include?(:step)
      self.step = args.first[:step]
      self.status = "pending"

      # Save the created resource immediately, since the linked resources are only available when persisted.
      # self.save!
    end
  end

  def destroy(*args)
    self.inputs.each(&:destroy)
    self.outputs.each(&:destroy)
    super(*args)
  end

  def save!(*args)
  #   self.inputs.each(&:save!)
  #   self.outputs.each(&:save!)
    self.update_repository
    super(*args)
  end

  def update_repository
    self.outputs.each do |output|
      self.repository << output.repository
    end
    self.inputs.each do |input|
      self.repository << input.repository
    end

  end

  def depended_step_instances
    @depended_step_instances || update_depended_step_instances
  end

  def update_depended_step_instances
    @depended_step_instances = self.workflow_instance.step_instances.select {|si| self.step.depended_steps.include?(si.step) }
  end

  def update_status # TODO: Should check the constraints and return false if one of them is not satisfied.

    return true if self.completed?
    return false unless self.conditional_dependencies_met?

    self.retrieve_inputs
    if self.step.human_step?
      self.status = 'ready'
      self.dump_if_cached
      return false # The SI is ready, but not completed yet.
    else
      self.execute
    end

    true
  end

  # Checks only the ConditionalControlPipes, since other Pipe types are already handled.
  def conditional_dependencies_met?
    self.step.incoming_pipes.select {|p| p.has_rdf_type?(POC_CORE.ConditionalControlPipe) }.each do |p|
      source_step_instance = self.workflow_instance.step_instances.find {|si| si.step == p.source_step }
      raise 'Step instance not found!' unless source_step_instance
      source_value_binding = source_step_instance.outputs.find {|vb| vb.label == p.source_port}
      raise 'Value to bind not found!' unless source_value_binding

      # If it's a TruePipe, but the value does not evaluate true,
      # or if it's a FalsePipe and the value evaluates false,
      # the check fails.
      return false if (p.has_rdf_type?(POC_CORE.TruePipe) ^ source_value_binding.value)
    end
    true
  end

  def retrieve_inputs
    self.step.incoming_pipes.each do |p|
      if p.has_rdf_type?(POC_CORE.HumanPipe)
        next # Do nothing!
      elsif p.has_rdf_type?(POC_CORE.DirectPipe)
        value = p.source_uri_value || p.source_literal_value
      elsif p.has_rdf_type?(POC_CORE.PortPipe)
        source_step_instance = self.workflow_instance.step_instances.find {|si| si.step == p.source_step }
        raise 'Step instance not found!' unless source_step_instance
        source_value_binding = source_step_instance.outputs.find {|vb| vb.label == p.source_port}
        raise 'Value to bind not found!' unless source_value_binding
        value = source_value_binding.value
      elsif p.has_rdf_type?(POC_CORE.ControlPipe)
        next # ControlPipes would not provide inputs.
      else
        raise 'Unrecognized Pipe type!'
      end

      add_input(p.target_port, value) if value.present?

    end
  end

  def execute
    # self.outputs = self.step.task.execute(self.inputs)
    puts self.inputs.size
    self.inputs.each do |input|
      puts "#{input.label} : #{input.value}"
    end

    self.outputs = self.step.execute(self.inputs)
    self.status = 'completed'
    self.dump_if_cached
    true
  end

  def perform_as_user(data, user)
    raise AuthorizationError unless self.allowed_for?(user)
    graph = RDF::Graph.new.from_ttl( data.gsub('<>', "<#{self.uri.to_s}>") )
    # POC::Auxiliary.replace_uri_in_graph!(graph, RDF::URI(''), self.uri)
    POC::Auxiliary.skolemize!(graph)

    graph.query(subject: self.uri, predicate: POC_SERVICES.input) do |statement|
      graph << RDF::Statement(statement.object, RDF.type, POC_CORE.ValueBinding)
      # vb_graph = graph.query(subject: statement.object).inject(RDF::Graph.new) { |g, s| g << s; g }
      # vb = ValueBinding.new(statement.object)
      # vb.hydrate!(graph: vb_graph)
      # vb.dump_to_cache
    end
    self.repository << graph
    self.save!
    self.dump_to_cache

    self.execute # TODO: Check if inputs updated properly.
    self.save! # TODO: Should this be saved here?
    self.dump_to_cache

    self.workflow_instance.update_status

    self
  end

  def completed?
    status == 'completed'
  end

  def ready?
    status == 'ready'
  end

  def add_binding(value_binding, type)
    if type == :input
      predicate = POC_SERVICES.input
    elsif type == :output
      predicate = POC_SERVICES.output
    else
      raise 'Binding type should be ":input" or ":output".'
    end

    self.repository << RDF::Statement.new(self.uri, predicate, value_binding.uri)

    if self.cached?
      value_binding.dump_to_cache
      self.dump_to_cache
    end
  end

  def add_input(name, value)
    value_binding = ValueBinding.new(name: name, value: value)
    self.add_binding(value_binding, :input)
  end

  def add_input!(name, value)
    self.add_input(name, value)
    self.save!
  end

  def add_output(name, value)
    value_binding = ValueBinding.new(name: name, value: value)
    self.add_binding(value_binding, :output)
  end

  def add_output!(name, value)
    self.add_output(name, value)
    self.save!
  end

  def value_bindings
    self.inputs + self.outputs
  end

  def basic_repo(opts={})
    repo = RDF::Repository.new
    repo << self.repository
    self.value_bindings.each do |value_binding|
      graph = value_binding.repository

      graph.query(predicate: RDF.type) do |statement|
        graph.delete statement
      end
      repo << graph
      POC::Auxiliary.replace_uri_in_graph!(repo, value_binding.uri, RDF::Node.new)
    end
    repo
  end

  # TODO: Implement!
  def allowed_for?(user=nil)
    true
  end

end

class Instance < Concept
  # Overwritten the constructor instead of this. Callback methods are stacked in the wrong order.
  # set_callback :initialize, :after, :set_creation_data
  # after_initialize :set_creation_data

  rdf_type POC_CORE.Instance.to_s
  field :created_at, DCTERMS.created.to_s, datatype: RDF::XSD.dateTime
  linked_field :creator, DCTERMS.creator, class_name: 'User'

  def initialize(*args)
    super(*args)
    set_creation_data
  end

  def basic_repo(opts={})
    repo = super(opts)
    self.repository.query(subject: self.uri, predicate: DCTERMS.created) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: DCTERMS.creator) do |statement|
      repo << statement
    end
    repo
  end

  private
  def set_creation_data
    # p "Instance.set_creation_data called."
    # p "Created at: #{self.created_at}"
    self.created_at ||= Time.current.iso8601 # xsd:dateTime comforts ISO8601.
    self.creator = POC::Auxiliary.current_user if self._creator_uri.blank? # TODO: Fix when the current user is referable.
  end

end
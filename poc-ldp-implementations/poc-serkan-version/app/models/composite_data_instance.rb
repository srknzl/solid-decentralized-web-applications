class CompositeDataInstance < DataInstance

  rdf_type POC_CORE.CompositeDataInstance.to_s
  linked_field :datatype, POC_CORE.datatype, class_name: 'CompositeDatatype'
  linked_field :field_values, POC_CORE.fieldValue, class_name: 'ValueBinding', multivalued: true

  # Returns a dublicate of the object. Not overwriting +dub+ for this, as it may be used for some internal operations.
  def duplicate(*args)
    new_object = super(*args)
    new_object.datatype = self.datatype.dup
    new_object.field_values = self.field_values.map(&:duplicate)
    new_object
  end

  # TODO : Test this.
  def ==(other)
    return false if self.class != other.class || self.datatype != other.datatype || self.field_values.count != other.field_values.count
    self.field_values.all? { |field_value| other.field_values.include? field_value }
  end

  def basic_repo(opts={})
    repo = RDF::Repository.new
    repo << self.repository

    repo.query(predicate: POC_PROTOTYPE.saved ) do |statement|
      repo.delete(statement)
    end

    self.field_values.each do |field|
      graph = field.repository
      graph.query(predicate: RDF.type) do |statement|
        graph.delete statement
      end
      repo << graph
      POC::Auxiliary.replace_uri_in_graph!(repo, field.uri, RDF::Node.new)
    end
    repo
  end

end
class CompositeDatatype < Datatype
  rdf_type POC_CORE.CompositeDatatype.to_s
  linked_field :data_fields, POC_CORE.dataField, class_name: 'DataField', multivalued: true
  # field :field_uris, POC_CORE.field.to_s, datatype: POC_CORE.DataField, is_uri: true, multivalued: true

  def basic_repo(opts={})
    repo = RDF::Repository.new
    repo << self.repository
    self.data_fields.each do |field|
      graph = field.repository

      graph.query(predicate: RDF.type) do |statement|
        graph.delete statement
      end
      repo << graph
      POC::Auxiliary.replace_uri_in_graph!(repo, field.uri, RDF::Node.new)
    end
    repo
  end


end
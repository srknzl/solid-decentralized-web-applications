class Workflow < Concept

  rdf_type POC_CORE.Workflow.to_s
  linked_field :pipes, POC_CORE.pipe, class_name: 'Pipe', multivalued: true
  linked_field :steps, POC_CORE.step, class_name: 'Step', multivalued: true
  linked_field :constraints, POC_CORE.constraint, class_name: 'Constraint', multivalued: true

  def self.available_resources_for(user)
    self.all.resources.select do |r|
      r.constraints.all? { |c| c.allows?(user) }
    end
  end

  def invoke(user)
    if self.allowed_for?(user)
      WorkflowInstance.new(workflow: self, user: user)
    else
      false
    end
  end

  #
  # def basic_repo(user = nil)
  #   repo = super(user)
  #   repo
  # end

  def allowed_for?(user)
    self.constraints.all? {|c| c.allows? user}
  end

end
class User < Concept
  rdf_type POC_SERVICES.User.to_s
  linked_field :roles, POC_SERVICES.role, class_name: 'Role', multivalued: true
end
class Task < Concept
  rdf_type POC_CORE.Task.to_s

  # linked_to :inputs, POC_SERVICES.input.to_s, class_name: 'ValueBinding', multivalued: true
  # linked_to :outputs, POC_SERVICES.output.to_s, class_name: 'ValueBinding', multivalued: true



  def perform(inputs)

    case self.uri
      when POC_CORE.Create
        TaskExecution::Create.perform(inputs)
      when POC_CORE.Insert
        TaskExecution::Insert.perform(inputs)
      when POC_CORE.Remove
        TaskExecution::Remove.perform(inputs)
      when POC_CORE.Get
        TaskExecution::Get.perform(inputs)
      else
        raise 'Undefined Task URI.'
    end

  end
  alias_method :execute, :perform

end
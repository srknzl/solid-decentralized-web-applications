class ValueBinding < Concept
  # Identifies the port with the rdfs:label field.
  rdf_type POC_CORE.ValueBinding.to_s
  field :literal_value, POC_CORE.literalValue.to_s
  field :uri_value, POC_CORE.uriValue.to_s, is_uri: true

  def initialize(*args)
    super(*args)
    if args.size > 0 and args.first.is_a?(Hash)
      self.label = args.first[:name]
      self.value = args.first[:value]
    end
  end

  def value
    self.uri_value || self.literal_value
  end

  def value=(val)
    if val.is_a?(RDF::URI)
      self.uri_value = val
    else
      self.literal_value= val
    end
  end

  def duplicate(*args)
    new_object = super(*args)
    new_object.uri_value = self.uri_value.dup if self.uri_value
    new_object.literal_value = self.literal_value if self.literal_value
    new_object
  end

  def ==(other)
    self.class == other.class && self.literal_value == other.literal_value && self.uri_value == other.uri_value
  end

  # def basic_repo
  #   repo = RDF::Repository.new
  #   self.repository.query(subject: self.uri, predicate: POC_CORE.uriValue) do |s|
  #     repo << RDF::Statement(s.subject, POC_CORE.value, s.object)
  #   end
  #
  #   self.repository.query(subject: self.uri, predicate: POC_CORE.literalValue) do |s|
  #     repo << RDF::Statement(s.subject, POC_CORE.value, s.object)
  #   end
  #
  #
  # end

end
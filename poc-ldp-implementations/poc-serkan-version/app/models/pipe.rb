class Pipe < Concept
  rdf_type POC_CORE.Pipe.to_s

  linked_field :source_step, POC_CORE.sourceStep, class_name: 'Step'
  linked_field :target_step, POC_CORE.targetStep, class_name: 'Step'

  field :source_port, POC_CORE.sourcePort.to_s, datatype: RDF::XSD.string
  field :target_port, POC_CORE.targetPort.to_s, datatype: RDF::XSD.string

  field :source_uri_value, POC_CORE.sourceUriValue.to_s, is_uri: true
  field :source_literal_value, POC_CORE.sourceLiteralValue.to_s
end
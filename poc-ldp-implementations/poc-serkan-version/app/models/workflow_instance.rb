# Represents a single enactment of a Workflow.
#
class WorkflowInstance < Instance
  # +TSort+ is used for topological ordering of the step instances associated with the workflow instance.
  include TSort

  rdf_type POC_SERVICES.WorkflowInstance.to_s

  # Possible values for a Status are "ready", "ongoing", "pending", "completed", "aborted".
  field :status, POC_SERVICES.status.to_s

  linked_field :workflow, POC_CORE.datatype, class_name: 'Workflow'
  # linked_field :workflow, POC_CORE.workflow, class_name: 'Workflow'
  linked_field :step_instances, POC_SERVICES.stepInstance, class_name: 'StepInstance', multivalued: true

  # set_callback :initialize, :after, :set_workflow_instance_details
  # set_callback :destroy, :before, :destroy_step_instances
  # set_callback :save, :before, :save_step_instances


  # Creates an WorkflowInstance object associated with the given Workflow object.
  #
  # @example Create a new object.
  #   WorkflowInstance.new(workflow: Workflow.all.resources.first, user: A.current_user)
  #
  # @raise -
  #
  # @return [ true ] True is success.
  def initialize(*args)
    super(*args)

    if args.size == 0
      raise 'WorkflowInstance initialization failed. Pass a URI or Workflow object.'
    elsif args.size > 0 && args.first.is_a?(Hash) && args.first.include?(:workflow)
    # If a workflow is specified, we are constructing a brand new instance.
      self.status = "ongoing"
      self.creator = args.first[:user]
      self.workflow = args.first[:workflow]

      step_instance_array = []
      self.workflow.steps.each do |step|
        step_instance = StepInstance.new(step: step)
        self.repository << RDF::Statement.new( self.uri, POC_SERVICES.stepInstance, step_instance.uri)
        step_instance_array << step_instance
      end
      self.step_instances = step_instance_array

      pipes = self.workflow.pipes.select { |p| p.has_rdf_type?(POC_CORE.PortPipe) || p.has_rdf_type?(POC_CORE.ControlPipe) }
      pipes.each do |pipe|
        source_si = self.get_step_instance_for_step(pipe._source_step_uri)
        target_si = self.get_step_instance_for_step(pipe._target_step_uri)

        if dependencies[target_si.uri.to_s]
          dependencies[target_si.uri.to_s] << source_si.uri.to_s
        else
          dependencies[target_si.uri.to_s] = [source_si.uri.to_s]
        end
      end

      # Save the created resource immediately, since the linked resources are only available when persisted.
      # self.save!(without_step_instances: true)

      self.update_status
    end
  end

  def get_step_instance(uri)
    self.step_instances.find {|si| si.uri.to_s == uri.to_s}
  end

  def get_step_instance_for_step(uri)
    self.step_instances.find {|si| si.step.uri.to_s == uri.to_s}
  end

  def dependencies
    @dependencies ||= {}
  end

  def dependencies=(value)
    @dependencies = value
  end

  def destroy(*args)
    self.step_instances.all?(&:destroy)
    super(*args)
  end

  def update_repository
    self.step_instances.each do |si|
      si.update_repository
      self.repository << si.repository
    end
  end

  def save!(*args)
    # TODO: THIS IS NOT SAFE since the embedded resources are not removed from the database before their dumping.
    # We would be in trouble if the embedded resources are modified at this point.
    # For now, we can add a safe save option, enabled with a flag.
    self.update_repository
    super(*args)
  end

  def update_status # TODO: We should check for the time constraints here.
    # return false unless self.constraints.inject(true) {|p,c| p && c.check }

    self.tsort.map {|si_uri| self.get_step_instance(si_uri)}.all?(&:update_status)

    if self.step_instances.all? {|si| si.status == "completed"}
      self.status = "completed"
    elsif step_instances.find {|si| si.status == "aborted" }
      self.status = "aborted"
    end

    # TODO: Do we need to save WorkflowInstance objects within the initialization?
    # We are altering their state
    self.save!

  end

  def performable_step_instances_for(user)
    self.step_instances.map do |si|
      si.constraints.all? { |c| c.allows?(user) }
    end
  end

  def completed?
    self.status == 'completed'
  end

  # TSort methods

  def tsort_each_node(&block)
    self.step_instances.map {|si| si.uri.to_s}.each(&block)
  end

  def tsort_each_child(node, &block)
    (self.dependencies[node.to_s] || [] ).each(&block)
  end

  def available_step_instances(user=nil)
    self.step_instances.select { |s| s.status == 'ready' && s.allowed_for?(user)}
  end

  def step_instances_container(opts={})
    available_step_instances(opts[:user]).map(&:uri).inject(BasicContainer.new(opts)) do |container, uri|
      container << uri
    end
  end

  def basic_repo(opts={})
    repo = super(opts)
    self.repository.query(subject: self.uri, predicate: POC_SERVICES.status) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.datatype) do |statement|
      repo << statement
    end
    repo
  end

  def to_ldp_resource(opts)
    raise AuthorizationError unless opts[:user] && self.allowed_for?(opts[:user])

    resource = super(opts)
    si_container_uri = RDF::URI("#{resource.base_uri}/step_instances")
    resource.graph << RDF::Statement(resource.base_uri, POC_SERVICES.stepInstances, si_container_uri)
    resource.graph << step_instances_container(base_uri: si_container_uri).graph
    resource
  end

  def allowed_for?(user)
    self.workflow.allowed_for?(user)
  end

end
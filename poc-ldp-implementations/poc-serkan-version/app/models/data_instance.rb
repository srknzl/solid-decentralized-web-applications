class DataInstance < Instance
  rdf_type POC_CORE.DataInstance.to_s
  linked_field :datatype, POC_CORE.datatype, class_name: 'Datatype'

  def basic_repo(opts={})
    # self.class.descendants.each do |klass|
    #   if self.has_rdf_type?(klass.get_rdf_type)
    #     return klass.find(self.uri).basic_repo(opts)
    #   end
    # end
    object = self.get_most_specific_object

    if object == self
      super(opts)
    else
      object.basic_repo(opts)
    end

  end

  # Overwrites the generic method to enforce usage of the name 'DataInstance' by the objects of the descendant classes.
  def self.generate_uri(name = DataInstance.name, base_uri = SETTINGS.data_uri)
    POC::Auxiliary.generate_uri name, base_uri
  end

  def self.available_resources_for(user)
    self.where(saved: true).resources  # SERKAN: gives error
    #self.all.resources
  end

end

require_dependency 'image'
require_dependency 'list'
require_dependency 'composite_data_instance'

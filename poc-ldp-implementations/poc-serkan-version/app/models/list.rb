# Represent list data resources.
# Utilizes +RDF::List+.
class List < DataInstance
  # set_callback :initialize, :after, :get_or_create_list
  # set_callback :save, :before, :save_graph
  # set_callback :destroy, :before, :destroy_list_graph

  # set_callback :concept_initialization, :after, :get_or_create_list
  # after_initialize :get_or_create_list

  # We are not using the field method to define +@head+, since we want to
  # retrieve all the references to the list nodes in a single query,
  # which is needed for supporting blank nodes.
  # field :head, POC_CORE.items.to_s

  rdf_type POC_CORE.List.to_s

  def initialize(*args)
    super(*args)
    update_items
  end

  def update_items
    query = retrieve_items_query
    puts 'START: Sparql query to be executed'
    puts query
    puts 'END: Sparql query to be executed'
    ntriples_string = Tripod::SparqlClient::Query.query(query, "application/n-triples")
    graph = POC::Auxiliary._rdf_graph_from_ntriples_string(ntriples_string)

    @head = nil
    graph.query(subject: self.uri, predicate: POC_CORE.items) do |statement|
      @head = statement.object
    end
    # graph.insert([new_subject, RDF.rest, old_subject])
    if @head
      @items = RDF::List.new(@head, graph)
    else
      @items = RDF::List.new
      @head = @items.subject
    end
    @items
  end

  def save!(*args)
    super(*args)
    self.save_items
  end

  def destroy(*args)
    self.destroy_items
    super(*args)
  end

  def items=(new_items)
    @items = new_items
    self.update_head
  end

  def items
    @items || update_items
  end

  #
  # def head
  #   @head || update_head
  # end


  def update_head
    if @head != items.subject # TODO: Test!
      items.graph.query(subject: uri, predicate: POC_CORE.items) do |statement|
        items.graph.delete statement
      end
      @head = items.subject
      # This statement is stored in +items.graph+ since +@head+ might be a blank node, which would not be referable outside this graph.
      items.graph.insert([uri, POC_CORE.items, @head])
    end
  end

  def destroy_items
    query = destroy_items_query
    puts 'START: Sparql update query to be executed'
    puts query
    puts 'END: Sparql update query to be executed'
    Tripod::SparqlClient::Update::update(query)
  end

  def save_items
    update_head
    query = destroy_items_query + save_items_query
    puts 'START: Sparql update query to be executed'
    puts query
    puts 'END: Sparql update query to be executed'
    Tripod::SparqlClient::Update::update(query)
  end

  def destroy_items_query
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      DELETE {
        GRAPH <#{self.graph_uri}>
            {
        <#{self.uri.to_s}> <#{POC_CORE.items.to_s}> ?items.
            ?z rdf:first ?head;
        rdf:rest ?tail.
      }
    }
    WHERE {
      GRAPH <#{self.graph_uri}> {
        <#{self.uri.to_s}> <#{POC_CORE.items.to_s}> ?items.
        ?items rdf:rest* ?z.
        ?z rdf:first ?head;
           rdf:rest ?tail.
      }
    };
    "
  end


  def retrieve_items_query
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    CONSTRUCT {
      <#{self.uri.to_s}> <#{POC_CORE.items.to_s}> ?items.
      ?z rdf:first ?head;
      rdf:rest ?tail.
    }

    WHERE {
      GRAPH <#{self.graph_uri}> {
        <#{self.uri.to_s}> <#{POC_CORE.items.to_s}> ?items.
        ?items rdf:rest* ?z.
        ?z rdf:first ?head;
           rdf:rest ?tail.
      }
    }"
  end

  def save_items_query
    "INSERT DATA { GRAPH <#{self.graph_uri}> {
      #{self.items.graph.inject('') {|str, statement| str + statement.to_s + ' ' }}
    } };"
  end

  def <<(*args)
    self.items.<<(*args)
    self.update_head
    self.dump_if_cached
  end

  def [](*args)
    self.items.[](*args)
  end

  def insert(arg)
    raise 'Hash argument is required' unless arg.is_a?(Hash)
    index = arg[:index]
    item = arg[:item]
    return false unless item
    if index
      self.items = self.items.slice(0, index) + RDF::List[item] + self.items.slice(index, self.length - index)
    else
      self << item
    end
    self.dump_if_cached
  end

  def length
    self.items.length
  end

  alias :size :length

  def []=(*args)
    self.items.[]=(*args)
    self.update_head
    self.dump_if_cached
  end

  def shift(*args)
    self.items.shift(*args)
    self.update_head
    self.dump_if_cached
  end

  def unshift(*args)
    self.items.unshift(*args)
    self.update_head
    self.dump_if_cached
  end

  def last
    self.items.last
  end

  def remove(arg)
    if arg[:index]
      index = arg[:index]
      self.items = self.items.slice(0, index) + self.items.slice(index+1, self.length - index)
    elsif arg[:object]
      self.items = self.items - RDF::List[arg[:object]]
    else
      raise 'No index or item specified for removal!'
    end
    self.dump_if_cached
  end

  def duplicate(*args)
    new_list = super(*args)
    new_list.items = RDF::List[*self.items.to_a]
    new_list
  end

  def ==(other)
    self.class == other.class && self.items.to_a == other.items.to_a
  end

  def basic_repo(opts={})
    repo = RDF::Graph.new
    repo << self.repository
    repo << self.items.graph

    repo.query(predicate: POC_PROTOTYPE.saved ) do |statement|
      repo.delete(statement)
    end

    # TODO: This is not sound.
    # It seems that RDF::List data should not be skolemized. This is the reverse operation of skolemization, to enable proper serialization to Turtle.
    # Test if the skolemization breaks any basic operation.
    repo.each_statement do |statement|
      if statement.subject.to_s.include?('replaced_node')
        POC::Auxiliary.replace_uri_in_graph!(repo, statement.subject, RDF::Node.new)
      end
      if statement.object.to_s.include?('replaced_node')
        POC::Auxiliary.replace_uri_in_graph!(repo, statement.object, RDF::Node.new)
      end
    end

    repo
  end
end

#
# data_graph = RDF::Graph.new
# repo.query( [RDF::URI.new(u), :predicate, :object] ) do |statement|
#   data_graph << statement
#
#   if statement.object.is_a? RDF::Node
#     repo.query( [statement.object, :predicate, :object] ) {|s| data_graph << s}
#   end
# end
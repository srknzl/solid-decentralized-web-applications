class Concept
  # # 'rake middleware' complains about the absence of these methods.
  # def self.linked_to(*)
  # end
  # def self.linked_from(*)
  # end

  include Tripod::Resource
  include POC::Persistence
  include POC::LDP
  include POC::Errors
  extend POC::Finders
  extend POC::Links

  set_callback :destroy, :before, :remove_if_cached
  set_callback :save, :after, :dump_if_cached

  graph_uri SETTINGS.data_uri
  rdf_type POC_CORE.POC_Concept.to_s
  field :label, RDFS.label, datatype: RDF::XSD.string
  field :title, DCTERMS.title, datatype: RDF::XSD.string
  field :description, DCTERMS.description, datatype: RDF::XSD.string
  field :saved, POC_PROTOTYPE.saved, datatype: RDF::XSD.boolean

  # define_model_callbacks :concept_initialization
  # Instance methods
  def initialize(*args)

    if args.size == 0
      super(self.class.generate_uri.to_s)
    elsif args.size == 1 and args[0].is_a?(Hash)
      super(self.class.generate_uri.to_s, args[0])
    else
      super(*args)
    end

  end

  def duplicate(*args)
    new_object = self.class.new(*args)
    new_object.title = self.title
    new_object.label = self.label
    new_object.description = self.description
    new_object
  end

  def duplicate_of?(other)
    self == other && self.uri != other.uri
  end

  def has_rdf_type?(uri)
    ( self.rdf_type.is_a?(Array) and self.rdf_type.include?(uri)) or ( self.rdf_type == uri )
  end

  def has_uri?(uri)
    self.uri == uri
  end

  def save!(*args)

    self.class.ancestors.select {|k| k.respond_to? :get_rdf_type}.map(&:get_rdf_type).each do |class_uri|
      self.repository << RDF::Statement.new( self.uri, RDF.type, class_uri)
    end

    p "Save! called from #{self.class.name} instance with uri: #{self.uri}"
    super(*args)
  end

  def self.find_descendant(uri_s)
    uri_s = [uri_s] unless uri_s.is_a? Array

    self.descendants.reverse.each do |klass|
      return klass if uri_s.include? klass.get_rdf_type
    end
  end

  def get_most_specific_type
    self.class.descendants.find {|klass| self.has_rdf_type?(klass.get_rdf_type) } || self.class
  end

  def get_most_specific_object
    type = self.get_most_specific_type
    type == self.class ? self : type.find(self.uri, cached: true)
  end

  def basic_repo(opts={})
    repo = RDF::Repository.new
    self.repository.query(subject: self.uri, predicate: RDF.type) do |statement|
      repo << statement
    end

    self.repository.query(subject: self.uri, predicate: RDFS.label) do |statement|
      repo << statement
    end

    self.repository.query(subject: self.uri, predicate: DCTERMS.title) do |statement|
      repo << statement
    end

    self.repository.query(subject: self.uri, predicate: DCTERMS.description) do |statement|
      repo << statement
    end

    repo
  end

  def basic_repo_with_base_uri(opts={})
    base_uri = opts.delete(:base_uri)
    POC::Auxiliary.replace_uri_in_graph!(self.basic_repo(opts), self.uri, base_uri)
  end

  # def repo_with_base_uri(opts={})
  #   POC::Auxiliary.replace_uri_in_graph(self.repository, self.uri, opts[:base_uri])
  # end

  def to_ldp_resource(opts)
    resource = Resource.new(opts)
    resource.graph << self.basic_repo_with_base_uri(base_uri: resource.base_uri)
    resource
  end

  def self.available_resources_for(user)
    self.all.resources
  end

  def self.available_container_for(user, base_uri=nil)
    resources = self.available_resources_for(user)
    container = BasicContainer.new

    resources.each do |r|
      container <<  r.relative_uri(base_uri)
    end
    container
  end

end
class Image < DataInstance
  rdf_type POC_CORE.Image.to_s
  linked_field :datatype, POC_CORE.datatype, class_name: 'ImageType'
  field :path, POC_CORE.path.to_s, is_uri: true

  def duplicate(*args)
    new_object = super(*args)
    new_object.path = self.path.dup
    new_object.datatype = self.datatype.dup
    new_object
  end

  def ==(other)
    self.class == other.class && self.path == other.path && self.datatype == other.datatype
  end

  def duplicate_of?(other)
    self == other && self.uri != other.uri
  end


  def basic_repo(opts={})
    repo = super(opts)
    self.repository.query(subject: self.uri, predicate: POC_CORE.datatype) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.path) do |statement|
      repo << statement
    end
    repo
  end

end

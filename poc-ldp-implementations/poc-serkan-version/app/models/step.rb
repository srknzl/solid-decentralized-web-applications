class Step < Concept
  rdf_type POC_CORE.Step.to_s

  # linked_field :task, POC_CORE.task, class_name: 'Task'
  linked_field :input_ports, POC_CORE.inputPort, class_name: 'InputPort', multivalued: true
  linked_field :output_ports, POC_CORE.outputPort, class_name: 'OutputPort', multivalued: true
  linked_field :constraints, POC_CORE.constraint, class_name: 'Constraint', multivalued: true
  linked_back :workflow, :steps, class_name: 'Workflow'
  # linked_from :_workflows, :_steps, class_name: 'Workflow' # There is actually a single Workflow, however this returns a ResourceCollection, which acts like an array.

  field :view_template, POC_CORE.viewTemplate.to_s #, datatype: POC_CORE.ViewTemplate
  # field :description, DCTERMS.description.to_s, datatype: RDF::XSD.string

  def execute(inputs)
    StepExecution::Engine.execute(inputs, self.rdf_type)
  end

  def update_incoming_pipes
    @incoming_pipes = self.workflow.pipes.select { |p| p.target_step == self }
  end

  def incoming_pipes
    @incomimg_pipes || update_incoming_pipes
  end
  # Steps that are sources of the incoming pipes.
  def update_depended_steps
    @depended_steps = self.incoming_pipes.map {|p| p.source_step }.compact
  end

  def depended_steps
    @depended_steps || update_depended_steps
  end

  def human_pipes
    @human_pipes || update_human_pipes
  end

  def update_human_pipes
    @human_pipes = self.pipes.select  { |p| p.has_rdf_type?(POC_CORE.HumanPipe) }
  end

  def human_step?
    self.human_pipes.empty? ? false : true
  end

  def pipes
    @pipes || self.update_pipes
  end

  def update_pipes
    @pipes = self.workflow.pipes.select {|p| p.target_step == self }
  end

  def save!(*args)
    self.constraints.all?(&:save!)
    super(*args)
  end

  def destroy(*args)
    self.constraints.all?(&:destroy)
    super(*args)
  end

  def basic_repo(opts={})
    repo = RDF::Repository.new
    repo << self.repository
    (self.input_ports + self.output_ports).each do |p|
      graph = p.repository
      graph.query(predicate: RDF.type) do |statement|
        graph.delete statement
      end

      repo << graph

      POC::Auxiliary.replace_uri_in_graph!(repo, p.uri, RDF::Node.new)
    end

    repo
  end

end
class Constraint < Concept
  rdf_type POC_CORE.Constraint.to_s

  field :requirements, POC_CORE.requirement.to_s, multivalued: true
  linked_field :required_roles, POC_CORE.requiredRole, class_name: 'Role', multivalued: true

  def allows?(user)
    (self.required_roles - user.roles).empty?
  end

  def check
    true
  end

end
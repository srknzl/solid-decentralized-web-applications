class DerivedDatatype < Datatype
  rdf_type POC_CORE.DerivedDatatype.to_s
  linked_field :base_datatype, POC_CORE.baseDatatype, class_name: 'Datatype'

  # For DerivedDatatypes based on ImageType.
  field :max_width, POC_CORE.maxWidth.to_s, datatype: RDF::XSD.integer # Max width in pixels.
  field :min_width, POC_CORE.minWidth.to_s, datatype: RDF::XSD.integer
  field :max_height, POC_CORE.maxHeight.to_s, datatype: RDF::XSD.integer
  field :min_height, POC_CORE.minHeight.to_s, datatype: RDF::XSD.integer
  field :scale_width, POC_CORE.scaleWidth.to_s, datatype: RDF::XSD.integer
  field :scale_height, POC_CORE.scaleHeight.to_s, datatype: RDF::XSD.integer
  field :max_size, POC_CORE.maxSize.to_s, datatype: RDF::XSD.integer # Max file size in KBs.

  def basic_repo(opts={})
    repo = super(opts)

    self.repository.query(subject: self.uri, predicate: POC_CORE.baseDatatype) do |statement|
      repo << statement
    end

    self.repository.query(subject: self.uri, predicate: POC_CORE.maxWidth) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.minWidth) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.maxHeight) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.minHeight) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.scaleWidth) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.scaleHeight) do |statement|
      repo << statement
    end
    self.repository.query(subject: self.uri, predicate: POC_CORE.maxSize) do |statement|
      repo << statement
    end
    repo
  end

end

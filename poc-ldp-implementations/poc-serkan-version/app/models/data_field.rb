class DataField < Concept
  rdf_type POC_CORE.DataField.to_s
  field :field_type, POC_CORE.fieldType.to_s, is_uri: true
end
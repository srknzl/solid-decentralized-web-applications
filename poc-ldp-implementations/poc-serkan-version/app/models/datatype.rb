class Datatype < Concept
  rdf_type POC_CORE.Datatype.to_s

  # TODO: This violates a basic OOP principle: A parent class should be agnostic of its children.
  # TODO: This also does not work as expected in some cases, and is needed to be investigated further.
  def basic_repo(opts={})
    self.class.descendants.each do |klass|
      if self.has_rdf_type?(klass.get_rdf_type)
        return klass.find(self.uri).basic_repo(opts)
      end
    end
    super(opts)
  end

end

require_dependency 'primitive_datatype'
require_dependency 'derived_datatype'
require_dependency 'composite_datatype'
require_dependency 'collection'

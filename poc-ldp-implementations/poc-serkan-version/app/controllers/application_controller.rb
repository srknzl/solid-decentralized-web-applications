class ApplicationController < ActionController::Base
  protect_from_forgery

  def http_basic_auth
    #http_basic_authenticate_with :name => ENV['BASIC_AUTH_NAME'], :password => ENV['BASIC_AUTH_PASSWORD']

    authenticate_or_request_with_http_basic do |user, password|
      user == ENV['POC_AUTH_NAME'] && password == ENV['POC_AUTH_PASSWORD']
    end
  end

end
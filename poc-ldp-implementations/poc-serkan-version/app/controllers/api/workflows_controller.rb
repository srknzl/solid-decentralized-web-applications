class Api::WorkflowsController < Api::BaseController

  def index
    resource = Workflow.available_container_for(POC::Auxiliary.current_user, request.path[1..-1])
    render_ttl data: resource.to_ttl, status: :ok
  end

  def show
    raise 'Workflow not specified.' if params[:id].blank?
    workflow = Workflow.find_with_relative_uri "#{params[:id]}"
    raise AuthorizationError unless workflow.allowed_for?(POC::Auxiliary.current_user)

    resource = workflow.to_ldp_resource(user: POC::Auxiliary.current_user)
    render_ttl data: resource.to_ttl, status: :ok
  rescue Tripod::Errors::ResourceNotFound
    raise POC::Errors::ResourceNotFoundError
  end

  def invoke
    raise 'Workflow not specified.' if params[:id].blank?
    workflow = Workflow.find_with_relative_uri "#{params[:id]}"
    workflow_instance = workflow.invoke(POC::Auxiliary.current_user)
    raise AuthorizationError if workflow_instance.blank?
  
    # redirect_to(workflow_instance.relative_uri.to_s)
    render text: '', layout: false, content_type: 'text/turtle', status: :created
    render text: '', layout: false, content_type: 'text/turtle', status: :created
  
  end

  def create_instance
    raise 'uri parameter required.' unless params[:uri].present?
  
    wi = Workflow.create_instance(params[:uri]) # This returns a WorkflowInstance object.
  
    raise 'Error when creating an instance of the workflow.' unless wi
  
    performable_step_instances = wi.performable_step_instances_for(POC::Auxiliary.current_user).map { |si| si.uri.to_s }
  
    render json: {data: {actions: performable_step_instances}}, status: :ok
  
  end
  

end

class Api::StepsController < Api::BaseController

  def show
    raise 'Step not specified.' if params[:id].blank?
    s = Step.find_with_relative_uri "#{params[:id]}"

    # raise Errors::AuthorizationError unless wi.allowed_for?(POC::Auxiliary.current_user)

    resource = Resource.new
    resource.graph << POC::Auxiliary.replace_uri_in_graph!(s.basic_repo, s.uri, RDF::URI(''))

    render_ttl data: resource.to_ttl, status: :ok
  rescue Tripod::Errors::ResourceNotFound
    raise POC::Errors::ResourceNotFoundError
  end

  def index
    resource = Step.available_container_for(POC::Auxiliary.current_user, request.path[1..-1])
    render_ttl data: resource.to_ttl, status: :ok
  end
  # def created
  # end
  # def show
  # end
  # def create
  # end
  # def destroy
  # end


end

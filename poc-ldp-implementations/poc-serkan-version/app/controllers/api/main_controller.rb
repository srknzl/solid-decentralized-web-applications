class Api::MainController < Api::BaseController

  def home
    resource = Resource.new
    resource.graph << RDF::Statement.new(RDF::URI(''), RDF.type, POC_SERVICES.POC_Server)
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.workflows, RDF::URI("#{request.url}/workflows/"))
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.users, RDF::URI("#{request.url}/users/"))
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.datatypes, RDF::URI("#{request.url}/datatypes/"))
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.dataInstances, RDF::URI("#{request.url}/data_instances/"))
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.workflowInstances, RDF::URI("#{request.url}/workflow_instances/"))
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.stepInstances, RDF::URI("#{request.url}/step_instances/"))
    resource.graph << RDF::Statement.new(RDF::URI(''), POC_SERVICES.steps, RDF::URI("#{request.url}/steps/"))
    render_ttl data: resource.to_ttl, status: :ok
  end

  def ldp_test

    resource = Workflow.first

    repository = RDF::Repository.new
    ldprs = RDF::LDP::RDFSource.new('http://localhost:3000/', repository)
    ldprs.create('<http://localhost:3000/> <http://ex.org/prop> "moomin" .', 'text/turtle')
    p request.path
    p request.headers['REQUEST_URI']
    byebug
    # [200, {}, RDF::LDP::RDFSource.find(RDF::URI("http://localhost:3000/"), repository)]
    render_ttl data: repository.to_ttl, status: :ok
  end

  def unrecognized_url
    raise RoutingError
  end

end
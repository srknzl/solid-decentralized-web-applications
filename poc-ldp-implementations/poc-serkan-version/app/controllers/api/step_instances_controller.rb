class Api::StepInstancesController < Api::BaseController

  def index
    resource = StepInstance.available_container_for(POC::Auxiliary.current_user, request.path[1..-1])
    render_ttl data: resource.to_ttl, status: :ok
  end

  def show
    raise 'Step instance not specified.' if params[:id].blank?
    si = StepInstance.find_with_relative_uri "#{params[:id]}"
    resource = si.to_ldp_resource(user: POC::Auxiliary.current_user)
    render_ttl data: resource.to_ttl, status: :ok
  rescue Tripod::Errors::ResourceNotFound
    raise POC::Errors::ResourceNotFoundError
  end

  def perform
    raise 'Step instance not specified.' if params[:id].blank?
    request_body = request.body.read
    si = StepInstance.find_with_relative_uri "#{params[:id]}"
    si.perform_as_user(request_body.gsub(request.base_url, SETTINGS.data_uri), POC::Auxiliary.current_user)
    resource = si.to_ldp_resource(user: POC::Auxiliary.current_user)
    render_ttl data: resource.to_ttl, status: :ok
  end

  #
  # def created
  # end
  # def show
  # end
  # def create
  # end
  # def destroy
  # end

end

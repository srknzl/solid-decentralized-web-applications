class Api::DatatypesController < Api::BaseController

  def index
    resource = Datatype.available_container_for(POC::Auxiliary.current_user, request.path[1..-1])
    render_ttl data: resource.to_ttl, status: :ok
  end

  def show
    raise 'Datatype not specified.' if params[:id].blank?
    datatype = Datatype.find_with_relative_uri "#{params[:id]}"

    resource = datatype.to_ldp_resource(user: POC::Auxiliary.current_user)
    render_ttl data: resource.to_ttl, status: :ok
  rescue Tripod::Errors::ResourceNotFound
    raise POC::Errors::ResourceNotFoundError
  end

end

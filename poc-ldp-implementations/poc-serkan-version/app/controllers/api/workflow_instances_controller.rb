class Api::WorkflowInstancesController < Api::BaseController

  def index
    resource = WorkflowInstance.available_container_for(POC::Auxiliary.current_user, request.path[1..-1])
    render_ttl data: resource.to_ttl, status: :ok
  end

  def show
    raise 'Workflow instance not specified.' if params[:id].blank?
    wi = WorkflowInstance.find_with_relative_uri "#{params[:id]}"
    # raise AuthorizationError unless wi.allowed_for?(Auxiliary.current_user)
    resource = wi.to_ldp_resource(user: POC::Auxiliary.current_user, base_uri: wi.uri)
    render_ttl data: resource.to_ttl, status: :ok
  rescue Tripod::Errors::ResourceNotFound
    raise POC::Errors::ResourceNotFoundError
  end

  def create
    raise 'Workflow not specified.' if params[:id].blank?
    workflow = Workflow.find_with_relative_uri params[:id].to_s
    raise AuthorizationError unless workflow.allowed_for?(POC::Auxiliary.current_user)

    wi = WorkflowInstance.new user: POC::Auxiliary.current_user, workflow: workflow
    resource = wi.to_ldp_resource(user: POC::Auxiliary.current_user)
    render_ttl data: resource.to_ttl, status: :created, location: wi.relative_uri.to_s
  end

  def step_instances # TODO: This does not return the completed and available StepInstances.
    raise 'Workflow instance not specified.' if params[:id].blank?
    wi = WorkflowInstance.find_with_relative_uri "#{params[:id]}"
    # raise AuthorizationError unless wi.allowed_for?(Auxiliary.current_user)
    resource = wi.step_instances_container(user: POC::Auxiliary.current_user)
    render_ttl data: resource.to_ttl, status: :ok
  end


  # def destroy
  # end


end

class Api::BaseController < ActionController::Base
  include POC::LDP
  include POC::Errors
  include POC::Rendering

  protect_from_forgery
  # layout 'application'
  layout nil

  respond_to :ttl
  before_action :ensure_content_type_is_turtle!

  rescue_from AuthorizationError, with: :authorization_error
  rescue_from RoutingError, with: :routing_error
  rescue_from ResourceNotFoundError, with: :resource_not_found_error
  # rescue_from StandardError, with: :standard_error # TODO: Uncomment

  def resource_not_found_error
    render_ttl data: ErrorResource.new(description: 'Resource not found!').to_ttl, status: :not_found
  end

  def routing_error
    render_ttl data: ErrorResource.new(description: 'Routing error!').to_ttl, status: :not_found
  end

  def authorization_error
    render_ttl data: ErrorResource.new(description: 'Authorization error!').to_ttl, status: :unauthorized
  end

  def standard_error(error)
    puts error
    render_ttl data: ErrorResource.new(description: error).to_ttl, status: :unprocessable_entity
  end

  def ensure_email_param_exists
    ensure_param_exists :email
  end

  def ensure_auth_token_param_exists
    ensure_param_exists :auth_token
  end

  def ensure_password_param_exists
    ensure_param_exists :password
  end

  def ensure_id_param_exists
    ensure_param_exists :id
  end

  def ensure_param_exists(param)
    return true unless params[param].blank?
    raise "Missing body parameter: #{param}."
  end

  def ensure_email_header_exists
    request.headers['email'].blank? ? authentication_error : true
  end

  def ensure_auth_token_header_exists
    request.headers['auth_token'].blank? ? authentication_error : true
  end

  def ensure_header_exists(header)
    header = header.to_s
    return true unless request.headers[header].blank?
    raise "Missing header parameter: #{header}."
  end

  # Ensures that PUT or POST requests have 'text/turtle' as their content-type.
  def ensure_content_type_is_turtle!
    return true unless (request.post? or request.put?) and (request.headers['CONTENT_TYPE'] =~ "/text/turtle")
    raise "Content-Type header is not set to 'text/turtle'."
  end

end

class MainController < ApplicationController
  # before_filter :http_basic_auth
  def home

  end

  def load_spec
    POC::Auxiliary.flush_data_graph
    POC::Auxiliary.flush_cache
    # @api_relative = "/api_#{POC::Auxiliary.random_string(5)}"
    @api_relative = SETTINGS.api_namespace
    @api_url = "#{request.base_url}#{@api_relative}"

    spec_handler = SpecHandler.new
    spec_handler.load_uploaded_file(params[:file])
    # spec_content = params[:file].read
    # spec_handler.load(spec_content)
  end

end
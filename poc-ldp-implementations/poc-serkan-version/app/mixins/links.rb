module POC
  module Links
    def linked_back(name, field, options = {})
      multivalued = options[:multivalued]
      raise 'Multivalued back links are not supported!' if multivalued
      # instance_variable = "@_#{name.to_s}".to_sym
      uri_variable = "@_#{name.to_s}_uri".to_sym
      uri_field = "_#{name.to_s}_uri".to_sym
      tripod_getter_method = "_#{name.to_s}".to_sym
      tripod_back_getter_method = "_#{field.to_s}".to_sym
      retrieve_method = "_retrieve_and_dump_#{name.to_s}".to_sym

      attr_accessor uri_field

      self.linked_from(tripod_getter_method, tripod_back_getter_method, options)

      self.class_eval do
        self.send(:define_method, name) do
          self.class.find_cached(self.instance_variable_get(uri_variable), options[:class_name]) || self.send(retrieve_method)
        end

        self.send(:define_method, retrieve_method) do
          data = self.send(tripod_getter_method).first

          if data
            # If +uri_variable+ is present and we are executing this, the resource is not cached. So we cache it.
            if uri_variable.present?
              data.dump_to_cache
            end

            self.instance_variable_set(uri_variable, data.uri)
            self.dump_to_cache
          end
          data
        end
        private retrieve_method
      end
    end

    def linked_field(name, predicate, options = {})
      multivalued = options[:multivalued]
      # instance_variable = "@_#{name.to_s}".to_sym
      setter_method = "#{name}=".to_sym
      tripod_getter_method = "_#{name.to_s}".to_sym
      # tripod_setter_method = "#{tripod_getter_method}=".to_sym
      uri_getter_method = (multivalued ? "#{tripod_getter_method}_uris" : "#{tripod_getter_method}_uri").to_sym
      uri_setter_method = "#{uri_getter_method}=".to_sym
      retrieve_method = "_retrieve_and_dump_#{name.to_s}".to_sym
      back_links = "_back_links_for_#{name.to_s}".to_sym
      assign_back_links = "_assign_back_links_for_#{name.to_s}".to_sym

      self.linked_to(tripod_getter_method, predicate.to_s, options)

      self.class_eval do
        self.send(:define_method, name) do
          self.class.find_cached(self.send(uri_getter_method), options[:class_name]) || self.send(retrieve_method)
        end

        self.send(:define_method, retrieve_method) do
          data = self.send(tripod_getter_method)
          data = data.to_a if multivalued
          data_array = multivalued ? data : [data]
          self.send(assign_back_links, data_array)
          data_array.each(&:dump_to_cache)
          self.dump_to_cache
          data
        end

        self.send(:define_method, back_links) do
          linked_froms.select { |k,v| v.class_name == self.class.name && v.incoming_field_name == tripod_getter_method }.keys
        end

        self.send(:define_method, assign_back_links) do |linked_resources|
          self.send(back_links).each do |back_link_name|
            uri_setter = "#{back_link_name.to_s}_uri=".to_sym
            linked_resources.each do |value|
              value.send(uri_setter, self.uri) if value.respond_to?(uri_setter)
            end
          end
        end

        self.send(:define_method, setter_method) do |data|
          data_array = multivalued ? data : [data]
          self.send(assign_back_links, data_array)
          data_array.each(&:dump_to_cache)
          uri_s = multivalued ? data.map(&:uri) : data.uri
          # self.instance_variable_set(instance_variable, data)
          self.send(uri_setter_method, uri_s)
          self.dump_to_cache
          data
        end
        private retrieve_method, back_links, assign_back_links
      end
    end
  end
end

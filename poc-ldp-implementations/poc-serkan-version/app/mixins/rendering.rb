module POC
  module Rendering

    def render_ttl(opts)
      data = opts.delete(:data)
      puts data
      puts data.gsub(SETTINGS.data_uri, request.base_url)
      render json: opts.merge(text: data.gsub(SETTINGS.data_uri, request.base_url), content_type: 'text/turtle')
    end

  end
end

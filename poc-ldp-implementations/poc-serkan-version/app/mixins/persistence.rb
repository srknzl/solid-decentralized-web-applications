module POC
  module Persistence

    def relative_uri(base='')
      RDF::URI(self.uri.to_s.split("#{SETTINGS.data_uri}#{base}").last)
    end

    # A fixed ID is needed for redis.
    # URI of the resource is not sufficient here, since the same URI may be used to initialize objects of different classes.
    def id
      "#{self.class.name}/#{self.uri.to_s}"
    end

    def remove_cache
      Redis::Objects.redis.del(self.id)
    end

    def dump_to_cache
      Redis::Value.new(self.id, marshal: true).value = self
    end

    def cached?
      Redis::Objects.redis.keys.include?(self.id)
    end

    def remove_if_cached
      remove_cache if cached?
    end

    def dump_if_cached
      dump_to_cache if cached?
    end

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      # Returns a RDF::URI based on a name if provided, otherwise uses the name of the calling object.
      # # @example
      #   Concept.generate_uri
      def generate_uri(name = self.name, base_uri = SETTINGS.data_uri)
        POC::Auxiliary.generate_uri name, base_uri
      end

      def generate_id(uri, class_name = self.name)
        "#{class_name}/#{uri.to_s}"
      end

    end

  end
end

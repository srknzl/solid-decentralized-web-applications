module POC
  module Finders
    def find(uri, options = {})
      if options.is_a?(Hash) && options[:cached] && resource = find_cached(uri)
        resource
      else
        super(uri, options)
      end
    end

    def find_with_relative_uri(relative_uri)
      uri = self.uri_from_relative relative_uri
      self.find uri
    end

    def uri_from_relative(relative_uri)
      "#{SETTINGS.data_uri}#{SETTINGS.api_namespace}/#{self.name.pluralize.underscore}/#{relative_uri}"
    end

    def find_cached(uri_s, class_name = self.name)
      if uri_s.is_a?(Array)
        values = []
        uri_s.each do |uri|
          value = Redis::Value.new(self.generate_id(uri, class_name), marshal:true).value
          if value
            values << value
          else
            return false # Fails if one of the resources is not found in the cache.
          end
        end
        values
      else
        Redis::Value.new(self.generate_id(uri_s, class_name), marshal:true).value
      end
    end

  end
end

cd $FUSEKI_PATH
sh fuseki-server --config=$POC_GENERATOR_ROOT_PATH/db/config-inf-tdb.ttl 
echo 'NOTE: This command assumes that the following environment variables exist:
- FUSEKI_PATH: The path to the Fuseki directory, which can be downloaded from http://jena.apache.org/documentation/serving_data/#download-fuseki
- POC_GENERATOR_ROOT: The path to the POC Specifier project root directory.'

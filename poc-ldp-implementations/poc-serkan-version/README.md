# POC Generator

A prototype developed by Murat Seyhan as a part of his thesis studies.

## Installation

1. Install [docker-compose](https://docs.docker.com/compose/install/).
2. Run the following comamnd in the project directory.
         
        docker-compose up

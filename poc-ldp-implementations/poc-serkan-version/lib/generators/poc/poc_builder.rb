# TODO: 
# Reference to other generators in poc_generator/lib/generator here.
# Try running rails new with this builder, inside a generator of the Poc Specifier app.
# Create the apps in '~/rails_applications/generated_apps' 
# 
#

class AppBuilder < Rails::AppBuilder
  include Thor::Actions
  include Thor::Shell


  def append_gems
    
    say("Appending gems", :yellow)
    @generator.gem 'better_errors', group: [:development]
    @generator.gem 'binding_of_caller', group: [:development]

    @generator.gem 'coffee-script-source', '1.5.0', group: [:assets]
    @generator.gem 'tripod'
    run 'bundle install' 
  end

    

  def add_tripod_configuration
  
    say("Appending tripod configuration to each environment", :yellow)
   
    @generator.append_file 'config/environments/test.rb', <<-DOC
      
Tripod.configure do |config|
  config.update_endpoint = 'http://127.0.0.1:3030/poc/update'
  config.query_endpoint = 'http://127.0.0.1:3030/poc/sparql'
  config.timeout_seconds = 30
end
      
    DOC
 
    
    @generator.append_file 'config/environments/production.rb', <<-DOC

Tripod.configure do |config|
  config.update_endpoint = 'http://ec2-54-186-80-192.us-west-2.compute.amazonaws.com:3030/poc/update'
  config.query_endpoint = 'http://ec2-54-186-80-192.us-west-2.compute.amazonaws.com:3030/poc/sparql'
  config.timeout_seconds = 30  
end

    DOC

        
    @generator.append_file 'config/environments/development.rb', <<-DOC
      
Tripod.configure do |config|
  config.update_endpoint = 'http://127.0.0.1:3030/poc/update'
  config.query_endpoint = 'http://127.0.0.1:3030/poc/sparql'
  config.timeout_seconds = 30
end
      
    DOC
 
  end

  def add_routes
    @generator.route "root to: 'main#home'"
    @generator.route "get '/' => 'main#home', as: 'home'"
  end

  def add_controllers
    @generator.generate(:controller, 'Main home')
  end

  def remove_redundant_files
    say('Removing the redundant files.',:yellow)
    
    @generator.run 'rm public/index.html'  
  end

  def add_view
    #file 'app/views/main/home.html.erb', <<-DOC
    @generator.append_file 'app/views/main/home.html.erb', <<-DOC
     <h2> This is a test.</h2>
    DOC
  end
  
  def leftovers
    append_gems
    add_controllers
    add_routes
    add_view
    
    remove_redundant_files

    generate(:layout, 'admin')
    add_tripod_configuration
  
  end
end

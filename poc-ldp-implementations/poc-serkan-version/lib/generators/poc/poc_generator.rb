
# TODO :
# It might be better to have an entire application template in templates, instead of using a builder.

class PocGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)

  # class_option :stylesheet, type: :boolean, default: true, desc: 'Creates a new rails app.'
  argument :poc_name, type: :string, default: 'poc_application'

  # def generate_layout
  #   copy_file 'stylesheet.css', "public/stylesheets/#{file_name}.css" if options.stylesheet?
  #   template 'layout.html.erb', "app/views/layouts/#{file_name}.html.erb"
  # end

  def run_builder
    inside(application_dir) do
      run "rails new #{name} -b #{builder_path}"
    end

  end


  private

  def builder_path
    File.expand_path('../poc_builder.rb', __FILE__)
  end


  def application_dir
    File.expand_path('~/rails_applications/auto_generated/')
  end


  def name
    poc_name.underscore
  end

end

namespace :tests do
  desc "Investigating the problem in 'to_ttl' method."
  task :to_ttl => :environment do
    data = File.open(Rails.root.join('app', 'assets', 'graphs', 'to_ttl.ttl'), 'rb') { |f| f.read }
    graph = RDF::Graph.new
    # graph.from_ttl(data)
    graph.load(Rails.root.join('app', 'assets', 'graphs', 'to_ttl.ttl'), format: :ttl)
    graph.each_statement do |s|
      puts s.to_s
    end
    #puts 'Failure count creating merchants: ' + failure_count_merchants.to_s
  end

end

require 'test_helper'

class WorkflowInstanceTest < ActiveSupport::TestCase

  test 'Append item' do
    workflow = Workflow.find('http://example.org/ttt#AppendTest')
    assert_not_nil workflow, 'Workflow not found!'

    wf_instance = WorkflowInstance.new workflow: workflow, user: A.current_user

    assert_not_nil wf_instance, 'WorkflowInstance object could not be created!'

    assert wf_instance.completed?

    end

end


- In order to run this application without docker you need to do the following.

  - Make sure to install gem dependencies by calling bundle install.
  - Installing same versions may be necessary.
  - Install redis and keep it running in the background
  - Download, run make install on root directory, make sure to check if you can call redis-server after that, if not add redis directory to path
  - Add bin folder in apache jena to path as riot program in it is used
  - Install fuseki(If you did the previous step it is done), a sparql server, which is inside apache jena package. Add fuseki to path and likewise, keep it running. You can open sparql console and manage data at address localhost:3030
  - Go to localhost:3030 and create a dataset called "ds".
  - To start web server : rails server -e development. If you get an error saying already running server, delete all the files inside poc-serkan-version/tmp/pids. 

# +RDF::URI+ objects for the same URIs somehow produce different hash codes. Here we modify +RDF::URI+ to generate consistent hash codes.
# This is needed when querying the in-memory repositories. See line 335 in repository.rb.

module RDF
  class URI
    ##
    # Returns a hash code for this URI.
    #
    # @return [Fixnum]
    def hash
      return (to_s.hash * -1)
    end

    ##
    # Returns if a URI belongs to the XSD namespace.
    #
    # @return [Boolean]
    def belongs_to_xsd?
      term = RDF::Vocabulary.find_term(self)
      term && term.vocab == RDF::XSD
    end

  end
end

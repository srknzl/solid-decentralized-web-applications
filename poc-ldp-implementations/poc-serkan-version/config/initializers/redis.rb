require 'connection_pool'
# redis_path_array = ENV['REDIS_PORT'].split('://')
# redis_path_array.shift
# redis_path_array.unshift('http://')
# redis_host_array = redis_path_array.join.split(':')
# redis_host_array.pop
# redis_host = redis_host_array.join(':')
redis_port = 6379
redis_host = "redis"
Redis::Objects.redis = ConnectionPool.new(size: 5, timeout: 5) { Redis.new(:host => redis_host, :port => redis_port) }
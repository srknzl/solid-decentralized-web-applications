# encoding: utf-8

# Here we modify +Tripod+ to hydrate resources in the constructor.
# This is needed since we want the resources properly initialized when the +initialize+ callback methods are run.

# This module defines behaviour for finders.
module Tripod::Finders
  module ClassMethods

    # Find a +Resource+ by its uri (and, optionally, by its graph if there are more than one).
    #
    # @example Find a single resource by a uri.
    #   Person.find('http://ric')
    #   Person.find(RDF::URI('http://ric'))
    # @example Find a single resource by uri and graph
    #   Person.find('http://ric', :graph_uri => 'http://example.com/people')
    # @example Find a single resource by uri, looking in any graph (i.e. the UNION graph)
    #   Person.find('http://ric', :ignore_graph => true)
    # @example Find a single resource by uri and graph (DEPRECATED)
    #   Person.find('http://ric', 'http://example.com/people')
    #   Person.find(RDF::URI('http://ric'), Person.find(RDF::URI('http://example.com/people')))
    #
    # @param [ String, RDF::URI ] uri The uri of the resource to find
    # @param [ Hash, String, RDF::URI ] opts Either an options hash (see above), or (for backwards compatibility) the uri of the graph from which to get the resource
    #
    # @raise [ Tripod::Errors::ResourceNotFound ] If no resource found.
    #
    # @return [ Resource ] A single resource
    def find(uri, opts={})
      if opts.is_a?(String) # backward compatibility hack
        graph_uri = opts
        ignore_graph = false
      else
        graph_uri = opts.fetch(:graph_uri, nil)
        ignore_graph = opts.fetch(:ignore_graph, false)
      end

      if ignore_graph
        resource = self.new(uri, ignore_graph: true, hydrate: true)
      else
        graph_uri ||= self.get_graph_uri
        unless graph_uri
          # do a quick select to see what graph to use.
          select_query = "SELECT * WHERE { GRAPH ?g {<#{uri.to_s}> ?p ?o } } LIMIT 1"
          result = Tripod::SparqlClient::Query.select(select_query)
          if result.length > 0
            graph_uri = result[0]["g"]["value"]
          else
            raise Tripod::Errors::ResourceNotFound.new(uri)
          end
        end
        resource = self.new(uri, graph_uri: graph_uri.to_s, hydrate: true)
      end

      # check that there are triples for the resource (catches case when someone has deleted data
      # between our original check for the graph and hydrating the object.
      # raise Tripod::Errors::ResourceNotFound.new(uri) if resource.repository.empty? # Murat: This is not safe if the constructor is inserting some statements.
      raise Tripod::Errors::ResourceNotFound.new(uri) if resource.rdf_type.size == 0   # Murat: This is a workaround.

      # return the instantiated, hydrated resource
      resource
    end

    # given a graph of data, and a hash of uris=>graphs, create and hydrate some resources.
    # Note: if any of the graphs are not set in the hash,
    # those resources can still be constructed, but not persisted back to DB.
    def _resources_from_graph(graph, uris_and_graphs)
      repo = add_data_to_repository(graph)
      resources = []

      # TODO: ? if uris_and_graphs not passed in, we could get the
      # uris from the graph, and just not create the resources with a graph
      # (but they won't be persistable).

      uris_and_graphs.each do |(u,g)|
        g ||= {}
        # make a graph of data for this resource's uri
        data_graph = RDF::Graph.new
        repo.query( [RDF::URI.new(u), :predicate, :object] ) do |statement|
          data_graph << statement
          if statement.object.is_a? RDF::Node
            repo.query( [statement.object, :predicate, :object] ) {|s| data_graph << s}
          end
        end
        # instantiate a new resource
        r = self.new(u, graph_uri: g, hydrate: true, data_graph: data_graph)
        resources << r
      end
      resources
    end

  end
end

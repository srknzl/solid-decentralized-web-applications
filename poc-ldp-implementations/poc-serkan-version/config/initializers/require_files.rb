Dir[Rails.root.join('app', 'core', '**', '*.rb').to_s].each do |file|
  require file
end

Dir[Rails.root.join('app', 'mixins', '**', '*.rb').to_s].each do |file|
  require file
end

# Deserialization of Ruby objects requires the Class of the object to be loaded beforehand, therefore models are loaded here.
Dir[Rails.root.join('app', 'models','**', '*.rb').to_s].each do |file|
  require file
end

PocSpecifier::Application.routes.draw do
  root 'main#home'

  # get '/' => 'main#home', as: 'home'
  # get '/test' => 'main#test', as: 'test'
  # get '/task' => 'main#task', as: 'task'
  # # get '/new' => 'main#new', as: 'new'
  # get '/flush_aux' => 'main#flush_auxiliary_graph', as: 'flush_aux'

  post '/load_spec' => 'main#load_spec', as: 'load_spec'

  ## START: API Routes
  namespace :api, defaults: { format: :ttl }  do
    root 'main#home'
    resources :workflow_instances
    resources :workflows
    resources :step_instances
    resources :steps
    resources :datatypes
    resources :users
    resources :data_instances

    match '/test' => 'main#ldp_test', :via => [:get,:post], :as => 'ldp_test'

        post '/workflows/:id' => 'workflow_instances#create'

    get '/workflow_instances/:id/step_instances' => 'workflow_instances#step_instances'

    # TODO: PATCH method may be unsupported! LDP requires PUT requests to provide a complete representation of the resource.
    # patch '/step_instances/:id' => 'step_instances#perform'
    match '/step_instances/:id' => 'step_instances#perform', via: [:patch]

    match '/*dummy' => 'main#unrecognized_url',  :via => [:get], as: 'unrecognized_url'
  end
  ## END: API Routes

end

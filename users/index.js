const express = require("express");
const app = express();
const axios = require("axios").default;
const qs = require("querystring");
const N3 = require("n3");
const df = require("n3").DataFactory;

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "*");
  res.setHeader("Access-Control-Allow-Credentials", "false");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type,Cookie,Set-Cookie"
  );
  next();
});

app.get("/pocUsers.ttl", (req, res, next) => {
    const data = qs.stringify({
      query: "SELECT ?s ?p ?o WHERE { GRAPH <http://storytelling.users>{ ?s ?p ?o}}",
    });
  
    axios
      .post("http://fuseki-server:3030/ds/query", data, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
      .then((ress) => {
        const writer = new N3.Writer({
          prefixes: {
            "": "http://localhost:1212/pocUsers.ttl",
            acl: "http://www.w3.org/ns/auth/acl#",
            dc: "http://purl.org/dc/elements/1.1/",
            vcard: "http://www.w3.org/2006/vcard/ns#",
            xsd: "http://www.w3.org/2001/XMLSchema#",
          },
        });
        ress.data.results.bindings.forEach((x) => {
          if (x.s.value == "http://localhost:1212/pocUsers.ttl#poc") {
            writer.addQuad(
              df.namedNode("#poc"),
              df.namedNode(x.p.value),
              x.o.type == "literal"
                ? df.literal(x.o.value, df.namedNode(x.o.datatype))
                : df.namedNode(x.o.value)
            );
          } else {
            writer.addQuad(
              df.namedNode(x.s.value),
              df.namedNode(x.p.value),
              x.o.type == "literal"
                ? df.literal(x.o.value, df.namedNode(x.o.datatype))
                : df.namedNode(x.o.value)
            );
          }
        });
        writer.end((err, result) => {
          res.header("Content-Type", "text/turtle").status(200).send(result);
        });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send(err);
      });
  });

let port = 1212

app.listen(port, ()=>{
    console.log("Server started on port " + port)
})
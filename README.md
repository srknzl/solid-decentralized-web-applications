# Decentralized Purposeful Online Community Application Framework

* How to run? Go to poc-solid-app folder and follow intructions in the README.md in that folder.


* Project Specification and Introduction: [click](https://gitlab.com/srknzl/solid-decentralized-web-applications/-/wikis/Specification)
* The project report  [https://www.overleaf.com/read/kjscznbntfzw](https://www.overleaf.com/read/kjscznbntfzw)
* Project Requirements Document: [click](https://gitlab.com/srknzl/solid-decentralized-web-applications/-/wikis/Requirements)


## Description 

* This is a graduation project by Serkan Özel(me) advised by [Suzan Üsküdarlı](https://www.cmpe.boun.edu.tr/~uskudarli/). In this project we will build a solid community application. 


## What is Solid? 

* Solid is first appeared at 2016, so it is a relatively new project led by Tim Berners Lee, the founder of the web. The project takes place at MIT university. According to the description at solid.mit.edu: 

```
The project aims to radically change the way Web applications work today, resulting in true data ownership as well as improved privacy.
```

## What Solid offers?
 
 * **True data ownership**: Which means that the content is decoupled from the application and users are able to control their data by themselves. 
 * **Modular Design**: Solid prevents vendor lock-in by decoupling the data from the application. You can switch platform without worrying about losing your contacts (or any data) in the new platform.
 * **Reusing existing data**: The developers will be able to use existing data that was produced by other apps, causing the innovations easier.

## Status 

* You can see what is being done, what is done and what to do on [boards](https://gitlab.com/srknzl/solid-decentralized-web-applications/-/boards) page. 

